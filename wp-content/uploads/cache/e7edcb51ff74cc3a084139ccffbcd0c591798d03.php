<?php
$logos = rwmb_meta( 'logo', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$phone = rwmb_meta( 'phone', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$email = rwmb_meta( 'email', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];
$lang_args = array(
	'show_names' => 1,
	'show_flags' => 0, 
);
?>
<header class="">
<nav class="navigation">
	<div class="main-nav-container">
		<div class="container">
			<div class="menu-overlay"></div>
			<a class="navbar-brand" href="#"><img src="<?php echo e($header_image); ?>" alt="ADK"></a> 
		  	<div class="menu">
				<?php if(has_nav_menu('primary_navigation')): ?>
		    		<?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']); ?>

		  		<?php endif; ?>
		  	</div>
		  	<div class="menu__right">
		  		<div class="info_menu">
		  			<div class="info_menu__inner">
		  				<p class="info_menu__phone"><a href="tel:<?php echo e($phone); ?>"><?php echo e($phone); ?></a></p>
			  			<p class="info_menu__email"><a href="mailto:<?php echo e($email); ?>"><?php echo e($email); ?></a></p>
		  			</div>
			  	</div>
			  	<div class="lang_menu">
			  		<?php pll_the_languages($lang_args); ?>
			  	</div>
		  	</div>
		  	
		  	<button class="hamburger hamburger--squeeze" type="button">
			  	<span class="hamburger-box">
			    	<span class="hamburger-inner"></span>
			  	</span>
			</button>
		</div>

	</div>
</nav>
</header>