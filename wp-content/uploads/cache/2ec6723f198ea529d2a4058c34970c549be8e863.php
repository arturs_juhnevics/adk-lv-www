<?php $__env->startSection('content'); ?>
  <?php echo $__env->make('layouts.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <?php if(!have_posts()): ?>
    <div class="container">
    	<div class="notfound-page">
	       <?php if(rwmb_meta('404text_'.pll_current_language(), array( 'object_type' => 'setting',  'limit' => 1 ), 'settings')!='') : ?>
	            <?=rwmb_meta('404text_'.pll_current_language(), array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');?>
	        <?php else : ?>
	            <h1>
	                <span class="first">4</span>
	                <span class="second">0</span>
	                <span class="third">4</span>
	            </h1>
	            <h3><?=pll__('Page not found'); ?></h3>
	            <a href="<?php echo home_url(); ?>" class="button"><?=pll__('Go to homepage', 'General'); ?></a>
	        <?php endif ?>
        </div>
    </div>
  <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>