<?php $__env->startSection('content'); ?>
 <?php echo $__env->make('layouts.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php echo $__env->make('partials.home.category', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php

	$url = $_SERVER['REQUEST_URI'];
	$url_params = parse_url($url);

	if(isset($url_params['query'])) :
		parse_str($url_params['query'], $params); 
	endif;
	
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$postsPerPage = get_option( 'posts_per_page' );

 	$args = array(
        'post_type' => 'product',
        'orderby'=> 'title',
        'order' => 'ASC',
        'posts_per_page'   => $postsPerPage,
        'post_status'      => 'publish',
        'paged'          => $paged,
        
        
        'meta_query' => array(
            'relation' => 'OR'
           
        )

    );

    $args['tax_query'] = array();

    $array = array(
            array(
                'taxonomy' => 'product-category',
                'field' => 'slug',
                'terms' => get_queried_object()->slug,
            )
        );
        array_push($args['tax_query'], $array);

    if(isset($params['brand'])) :
		$brandArr = explode(',', $params['brand']);

		$array = array(
            array(
                'taxonomy' => 'brand-category',
                'field' => 'slug',
                'terms' => array_filter($brandArr),
            )
        );
        array_push($args['tax_query'], $array);
	endif;

	if(isset($params['solutions'])) :
		$brandArr = explode(',', $params['solutions']);
		$array = array(
                'key' => 'solution',
                'value' => $solutionArr[0],
                'compare' => '='
            );

       array_push($args['meta_query'], $array);
	endif;

	if(isset($params['cat'])) :
		$catArr = explode(',', $params['cat']);
		$array = array(
            array(
                'taxonomy' => 'product-category',
                'field' => 'slug',
                'terms' => array_filter($catArr),
            )
        );
        array_push($args['tax_query'], $array);
	endif;

    $query = new WP_Query( $args );
 ?>

  <div class="container product-container">
  	<div class="row">
  		<div class="col-sm-4">
	  		<?php echo $__env->make('partials.product.product-filter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	  	</div>
	  	<div class="col-sm-8">
	  		<div class="loading-overlay">
	  			<div class="loader">
	  				<?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/images/ripple-loader.svg' ); ?>
	  			</div>
	  		</div>
	  		<div class="row product-list">
	  			<?php 
					$terms = wp_get_post_terms( get_the_ID(), 'product-category');
				?>
		  		<?php while( $query->have_posts()): ?> <?php $query->the_post() ?>
		  			<div class="col-sm-4">
		  				<a href="<?php echo get_the_permalink( ); ?>">
							<div class="product-item">
								<div class="product-item__term">
									<p><?php echo $terms[0]->name; ?></p>
								</div>
								<div class="product-item__image" style="background-image:url(<?php echo get_the_post_thumbnail_url() ?>);">
								</div>
								<p class="product-item__name">
									<?php echo get_the_title( ); ?>
								</p>
							</div>
						</a>
		  			</div>
			   <?php endwhile; ?>
			   <div class="pager">
			        <?php
			        $url_params_regex = '/\?.*?$/';
					preg_match($url_params_regex, get_pagenum_link(), $url_params);
					$big = 999999999; // need an unlikely integer

					$base   = str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) );
					$format = 'page/%_%';

			       
			        $pages = paginate_links( array(
			            'base' => $base,
			            'format' => $format,
			            'current' => max( 1, get_query_var('paged') ),
			            'total' => $query->max_num_pages,
			            'prev_text'          => '<i class="fa fa-caret-left"></i>',
			            'next_text'          => '<i class="fa fa-caret-right"></i>',
			            'type'  => 'array',
			        ) );

			        if( $pages ) {

			            echo '<ul class="pager__list">';
			            foreach ( $pages as $page ) {
			                echo '<li class="pager__item">'.$page.'</li>';
			            }
			            echo '</ul>';
			        }
			        ?>
			    </div>
			</div>

	  	</div>
  	</div>
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>