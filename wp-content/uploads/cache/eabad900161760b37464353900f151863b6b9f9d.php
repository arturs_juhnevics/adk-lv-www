<?php $__env->startSection('content'); ?>
 <?php echo $__env->make('layouts.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php echo $__env->make('partials.home.category', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

 <?php
	function getCurrentCatID(){
	    global $wp_query;
	    if(is_category() || is_single()){
	        $cat_ID = get_query_var('cat');
	    }else{
	    	$cat_ID = "";
	    }
	    return $cat_ID;
	}
	$current_category_ID =  getCurrentCatID();

	$url = $_SERVER['REQUEST_URI'];
	$url_params = parse_url($url);
	if(isset($url_params['query'])) :
		parse_str($url_params['query'], $params); 
	endif;
	
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$postsPerPage = get_option( 'posts_per_page' );

 	$args = array(
        'post_type' => 'product',
        'order' => 'menu_order',
        'posts_per_page'   => $postsPerPage,
        'post_status'      => 'publish',
        'paged'          => $paged,
        'cat' => $current_category_ID,
        'meta_query' => array(
            'relation' => 'OR'
           
        )

    );

    $args['tax_query'] = array();

    if(isset($params['brand'])) :
		$brandArr = explode(',', $params['brand']);

		$array = array(
            array(
                'taxonomy' => 'brand-category',
                'field' => 'slug',
                'terms' => array_filter($brandArr),
            )
        );
        array_push($args['tax_query'], $array);
	endif;

	if(isset($params['solutions'])) :
		$solutionArr = explode(',', $params['solutions']);
		$array = array(
                'key' => 'solution',
                'value' => $solutionArr[0],
                'compare' => '='
            );

       array_push($args['meta_query'], $array);
	endif;



    $query = new WP_Query( $args );
 ?>

  <div class="container product-container">
  	<div class="row">
  		<div class="col-sm-4">
	  		<?php echo $__env->make('partials.product.product-filter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	  	</div>
	  	<div class="col-sm-8">
	  		<div class="loading-overlay">
	  			<div class="loader">
	  				<?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/images/ripple-loader.svg' ); ?>
	  			</div>
	  		</div>
	  		<div class="row product-list">
		  		<?php while( $query->have_posts() ): ?> <?php $query->the_post() ?>
		  			<div class="col-sm-4 products">
		  				<?php echo $__env->make('partials.product.product-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		  			</div>
			   <?php endwhile; ?>
			   <div class="pager">
			        <?php
			        $url_params_regex = '/\?.*?$/';
					preg_match($url_params_regex, get_pagenum_link(), $url_params);
					$big = 999999999; // need an unlikely integer

					$base   = str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) );
					$format = 'page/%_%';

			       
			        $pages = paginate_links( array(
			            'base' => $base,
			            'format' => $format,
			            'current' => max( 1, get_query_var('paged') ),
			            'total' => $query->max_num_pages,
			            'prev_text'          => file_get_contents(get_template_directory().'/assets/images/adk-icon-bulta-right-red.svg'),
          				'next_text'          => file_get_contents(get_template_directory().'/assets/images/adk-icon-bulta-right-red.svg'),
			            'type'  => 'array',
			        ) );

			        if( $pages ) {

			            echo '<ul class="pager__list">';
			            foreach ( $pages as $page ) {
			                echo '<li class="pager__item">'.$page.'</li>';
			            }
			            echo '</ul>';
			        }
			        ?>
			    </div>
			</div>
			
	  	</div>
  	</div>
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>