<?php $__env->startSection('content'); ?>
 <?php echo $__env->make('layouts.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <div class="container solution-list row">
   <?php while(have_posts()): ?> <?php the_post() ?>
  		<?php echo $__env->make('partials.solutions.solution-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   <?php endwhile; ?>
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>