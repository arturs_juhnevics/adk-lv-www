<div class="container partners block">
	<div class="block__heading animate animate__fade-up">
		<div class="row">
			<div class="col-12"><h2><?php echo rwmb_meta('Partners_title'); ?></h2></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<p class="partners__short animate animate__fade-up"><?php echo rwmb_meta('Partners_text'); ?></p>
			<?php 
				$partners = rwmb_meta('partenrs');
			?>
			<div class="partners__list">
			<?php foreach ($partners as $item) { ?>
				<div class="partners__list__item clearfix animate animate__fade-up">
					<?php	

						$name = (isset($item['partners_title'])) ? $item['partners_title'] : "partner"; 
						$image_ids = $item['partners_image'];
             			$image = RWMB_Image_Field::file_info( $image_ids[0], array( 'size' => 'medium' ));
						?>
					
						<img alt="<?php echo e($name); ?>" src="<?php echo e($image['url']); ?>"/>
					
				</div>
			<?php } ?>
			</div>
		</div>
		
	</div>
</div>