<?php
$solutions = get_posts( array(
    'post_type' => 'solution',
    'numberposts' => 5,
    'post_status' => 'publish',
) );
?>
<div class="container solutions block">
	<div class="block__heading">
		<div class="row">
			<div class="col-6 block__heading__left animate animate__fade-up"><h2><?php echo pll__('Risinājumi'); ?></h2></div>
			<div class="col-6 block__heading__right animate animate__fade-up"><a class="button--read-more" href="<?php echo rwmb_meta('solutions_url'); ?>"><?php echo pll__('Skatīt visus'); ?></a></div>

		</div>
	</div>
	<div class="solutions__slider animate animate__fade-up">
		<?php $__currentLoopData = $solutions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $solution): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php
			$image = get_the_post_thumbnail_url($solution->ID, 'large');
			$title = get_the_title($solution->ID);
			$short = rwmb_meta('solution_short_text', true, $solution->ID);
			$url = get_the_permalink($solution->ID);
		?>
		<div class="solutions__slider__item">
			<div class="row">
				<div class="col-sm-6">
					<div class="solutions__slider__item__image" style="background-image: url(<?php echo e($image); ?>)">
						<span class="slider_paging"></span>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="solutions__slider__item__info">
						<h3><?php echo e($title); ?></h3>
						<p><?php echo e($short); ?></p>
						<a href='<?php echo e($url); ?>' class="button--read-more"><?php echo pll__('Lasīt vairāk'); ?></a>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</div>
</div>
