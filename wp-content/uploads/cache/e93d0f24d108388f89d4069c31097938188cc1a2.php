<?php
$tax = get_query_var('taxonomy');
$getTerm =  get_query_var('term');
$queriedObejct = get_queried_object();
$taxonomyBrands = get_taxonomy( 'brand-category');

$termsBrands = get_terms( 'brand-category', array(
    'taxonomy' => $queriedObejct->name,
    'hide_empty' => true,

) );


if( isset($queriedObejct->term_id)) :


/**
 * Get reminding products in each term
 */
$taxonomies = array(
  'brand-category',

);

$termsArray = array();

foreach ($taxonomies as $tax){

  $terms = get_terms( array(
    'taxonomy' => $tax,
    'hide_empty'  => false,
  ));

  foreach ($terms as $term){
    $postIdsOfTerm = get_posts(array(
      'post_type' => 'product',
      'numberposts' => -1,
      'tax_query' => array(
        array(
          'taxonomy' => $tax,
          'field' => 'id',
          'terms' => $term->term_id,
          'include_children' => false
        )
      )
    ));
    $postid = "";
    foreach ($postIdsOfTerm as $item){
      $postid .= $item->ID." ";

    }
    $termsArray[$term->term_id] = $postid;

  }
}

$termCount = [];
$productIds = get_posts(array(
    'post_type' => 'product',
    'fields'          => 'ids', // Only get post IDs
    'posts_per_page'  => -1,
    'tax_query' => array(
        array(
          'taxonomy' =>"product-category",
          'field' => 'id',
          'terms' => $queriedObejct->term_id,
          'include_children' => false
        )
      )

));

foreach ($termsArray as $key => $value){
  $termArray = explode(" ", $termsArray[$key]);
  $termArray = array_filter($termArray);
  // $ids = array_filter($ids);

  $count  = count(array_intersect($productIds, $termArray));
  $termCount[$key] = $count;


}

endif;

global $post;
$args = array('post_type' => 'solution', 'posts_per_page' => 5);
$solutions = get_posts( $args );
?> 
<div class="filter-button animate animate__fade-up">
  <p><span><?php echo pll_e('Filtrs: ', 'Produkti'); ?></span><?php echo pll_e('Atlasīt preces', 'Produkti'); ?></p>
</div>
<div class="product-filter closed">
<div class="filter-back">
  <p><?php echo pll_e('Atpakaļ', 'Produkti'); ?></p>
</div>
<div class="product-filter__heading animate animate__fade-up">
  <p class="product-filter__heading__title"><?php echo pll_e('Atlasīt preces', 'Produkti'); ?></p>
  <a class="product-filter__heading__text button--light" id="cancel-all"><?php echo pll_e('Atcelt visus', 'Produkti'); ?></a>
</div>

<div class="accordion product-filter__inner" id="product-filter">
<?php if( is_tax() ) : ?>
   <div class="card animate animate__fade-up">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#category" aria-expanded="true" aria-controls="category">
          <?php echo pll_e('Apakštips', 'Produkti'); ?>
        </button>
      </h5>
    </div>

    <div id="category" class="collapse show category" aria-labelledby="headingOne" data-parent="#category">
      <div class="card-body product-filter__contents">
        <?php 
          $args = array('parent' => $queriedObejct->term_id);
          $categories = get_terms('product-category', $args );
        ?>
        <?php foreach ($categories as $item) { ?>
          <?php if( $item->count != 0 ) : ?>
            <div class="product-filter__contents__item">
                <input name="category" class="filter-checkbox" type="checkbox" data-page="1" data-filter="category" value="<?php echo $item->slug; ?>" id="<?php echo $item->term_id; ?>" >
                <label for="<?php echo $item->term_id; ?>"><?php echo $item->name; ?> (<?php echo $item->count; ?>)</label>
            </div>
          <?php endif; ?>
        <?php } ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <div class="card animate animate__fade-up">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#brands" aria-expanded="true" aria-controls="brands">
          <?php echo pll_e('Ražotāji', 'Produkti'); ?>
        </button>
      </h5>
    </div>

    <div id="brands" class="collapse show brands" aria-labelledby="headingOne" data-parent="#product-filter">
      <div class="card-body product-filter__contents">
      	<?php foreach ($termsBrands as $item) { ?>
          <?php if(isset($queriedObejct->term_id)) : ?>
            <?php if( $termCount[$item->term_id] != 0 ) : ?>
              <div class="product-filter__contents__item">
                  <input name="brand" class="filter-checkbox" type="checkbox" data-page="1" data-filter="brand" value="<?php echo $item->slug; ?>" id="<?php echo $item->term_id; ?>" >
                  <label for="<?php echo $item->term_id; ?>"><?php echo $item->name; ?> (<?php if(isset($queriedObejct->term_id)) { echo $termCount[$item->term_id]; }else{  echo $item->count; } ?>)</label>
              </div>
            <?php endif; ?>
          <?php else : ?>
            <?php if( $item->count != 0 ) : ?>
              <div class="product-filter__contents__item">
                  <input name="brand" class="filter-checkbox" type="checkbox" data-page="1" data-filter="brand" value="<?php echo $item->slug; ?>" id="<?php echo $item->term_id; ?>" >
                  <label for="<?php echo $item->term_id; ?>"><?php echo $item->name; ?> (<?php echo $item->count; ?>)</label>
              </div>
            <?php endif; ?>
          <?php endif; ?>
      	<?php } ?>
      </div>
    </div>
  </div>
  
  <div class="card animate animate__fade-up">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#brands" aria-expanded="true" aria-controls="brands">
          <?php echo pll_e('Risinājumi', 'Produkti'); ?>
        </button>
      </h5>
    </div>

    <div id="solutions" class="collapse show brands" aria-labelledby="headingOne" data-parent="#product-filter">
      <div class="card-body product-filter__contents">
        <?php foreach ($solutions as $post) : ?>

          <?php 
   
            $args = array(
                'post_type' => 'product',
                'posts_per_page'   => -1,
                'post_status'      => 'publish',
                
                'meta_query' => array(
                    'relation' => 'OR',
                    array(
                      'key' => 'solution',
                      'value' => $post->ID,
                      'compare' => '='
                    )
                )
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->found_posts != 0 ) : ?>
            <div class="product-filter__contents__item">
                <input name="solutions" data-page="1" data-filter="solutions" type="radio" value="<?php echo $post->ID; ?>" id="<?php the_title(); ?>" >
                <label for="<?php the_title(); ?>"><?php the_title(); ?> (<?php echo $query->found_posts; ?>)</label>
            </div>
          <?php endif; ?>
        <?php endforeach; 
        wp_reset_postdata();?> 
      </div>
    </div>
    <?php 
      if( is_tax() ) :
        $term = $getTerm;
      else :
        $term = 'false';
      endif; 
    ?>
      <input id="curTerm" type="hidden" value="<?php echo $term; ?>">
      <input id="curTax" type="hidden" value="<?php echo $tax; ?>">
  </div>
</div>

</div>