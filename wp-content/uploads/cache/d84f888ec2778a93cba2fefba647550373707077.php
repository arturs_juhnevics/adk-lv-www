<?php
$slides = get_posts( array(
    'post_type' => 'slider',
    'numberposts' => -1,
    'post_status' => 'publish',
) );

?>
<div class="hero">
<?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php
	$image = get_the_post_thumbnail_url($slide->ID);

	$title = get_post_meta($slide->ID, 'slider_title');
	$text = get_post_meta($slide->ID, 'slider_text');
	$buttonText = get_post_meta($slide->ID, 'slider_button');
	$buttonUrl = get_post_meta($slide->ID, 'slider_button_url');

	?>
	
	<div class="hero__item" style="background-image: url(<?php echo e($image); ?>)">
		<div class="container">
			<div class="hero__item__inner">
				<h2 class="hero__title"><?php echo e($title[0]); ?></h2>
					<div class="hero__text"><?php echo e($text[0]); ?></div>
					<?php if($buttonText): ?>
						<a href="<?php echo e($buttonUrl[0]); ?>" class="button"><?php echo e($buttonText[0]); ?></a> 
					<?php endif; ?>
			</div>
		</div>
	</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>