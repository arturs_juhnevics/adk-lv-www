<footer class="footer">
  <div class="container">
  	<footer class="footer__menu">
  		<div class="row">
            <?php 
                $taxonomy = 'product-category';
                $pterms = get_terms( $taxonomy,  array('parent' => 0) ); 
  
                ?>
                <?php  ?>
                  <?php foreach ($pterms  as $pterm ) : ?>
                   <div class="col-sm-3 animate animate__fade-up">
                        <p class="footer__menu__title"><?php echo $pterm->name; ?></p>
                        <ul>
                            <?php $terms = get_terms( $taxonomy, array( 'parent' => $pterm->term_id ) ); ?>
                            <?php foreach( $terms as $term ) : ?>
                                <li class="footer__menu__item"><a href="/product/<?php echo $pterm->slug. '/?&cat=' . $term->slug;  ?>"><?php echo $term->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                  <?php endforeach; ?>
                <?php  ?>
    	</div>
  	</footer>
    
  </div>
  <div class="footer-bottom clearfix animate animate__fade-up">
  		<div class="container clearfix">	
  			<p class="footer-bottom__copy">© <?php echo date("Y"); ?>  SIA ADK | 3M produkcija | Visas tiesības aizsargātas</p>
    		<p class="footer-bottom__design">Lapu izstrādāja: Minimo Digital Agency</p>
  		</div>
    </div>
</footer>
