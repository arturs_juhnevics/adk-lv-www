<div class="entry-content">
	 <?php while(have_posts()): ?> <?php the_post() ?>
	 	<div class="entry-content__featured-image" style="background-image: url(<?php echo get_the_post_thumbnail_url();  ?>)" >
	 	</div>
		<?php the_content(); ?>
	 <?php endwhile; ?>


</div>
<?php $products = rwmb_meta('solution_product');?>
<div class="container">
	<div class="related-products">
		<h2>Saistītie prodokti</h2>
		<div class="row">
			<?php 
				$args = array(
				'post_type'        => 'product',
				'meta_query' => array(
				       array(
				           'key' => 'solution',
				           'value' => get_the_ID(),
				           'compare' => '=',
				       )
				   )
				);
				$query = new WP_Query( $args ); 
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
					$query->the_post(); ?>
					
					<div class="col-sm-3">
						<?php echo $__env->make('partials.product.product-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
					</div>
					<?php } // end while
				} // end if
				wp_reset_query();
			?>
		</div>
	</div>
</div>