<?php

class Product{

    public function __construct() {

        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'template-product.php'
        ));
        $page = reset($pages);
        $pageName = isset($page->post_name) ? $page->post_name : 'product';


        $labels = array(
            'name'               => "Produkti",
            'singular_name'      => "Product",
            'menu_name'          => "Products",
            'add_new_item'       => "Add Product",
            'new_item'           => "New Product",
            'edit_item'          => "Edit Product",
            'view_item'          => "View Product",
            'all_items'          => "All Products",
            'search_items'       => "Search Products",
            'not_found'          => "No Product found",
            'not_found_in_trash' => "No Product found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => true,
            'supports' => array('thumbnail', 'editor', 'title'),    
            'rewrite'      => array('slug' => 'produkti', 'with_front' => true)  
        );

        register_post_type('product', $args);

        add_filter( 'rwmb_meta_boxes', 'product_single_meta_boxes' );

        function product_single_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'product',
                'title'      => 'Product',
                'post_types' => 'product',  
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(

                    array(
                        'name'        => 'Select a solution',
                        'id'          => 'solution',
                        'type'        => 'post',

                        'multiple'        => true,
                        // Post type.
                        'post_type'   => 'solution',

                        // Field type.
                        'field_type'  => 'select_advanced',

                        // Placeholder, inherited from `select_advanced` field.
                        'placeholder' => 'Select a solution',

                        // Query arguments. See https://codex.wordpress.org/Class_Reference/WP_Query
                        'query_args'  => array(
                            'post_status'    => 'publish',
                            'posts_per_page' => - 1,
                        ),
                    ),
                  
                    array(
                        'id'               => 'product_gallery',
                        'name'             => 'Gallery',
                        'type'             => 'image_advanced',
                        'max_status'       => 'false',
                        'image_size'       => 'thumbnail',
                    ),     
                    array(
                        'id'               => 'product_extra',
                        'name'             => 'Extra description',
                        'type'             => 'wysiwyg',
                    ), 
                    array(
                        'id'               => 'product_tech',
                        'name'             => 'Technical data',
                        'type'             => 'wysiwyg',
                    ),         
                     array(
                        'id'               => 'product_files',
                        'name'             => 'Downloadable files',
                        'type'             => 'file_upload',
                        'max_status'       => 'false',
                        'image_size'       => 'thumbnail',
                    ), 
                )
            );

            return $meta_boxes;
        }

       //Product categories
        $labels = array(
            'name' => _x('Product Categories', 'taxonomy general name', 'admin'),
            'singular_name' => _x('Product Category', 'taxonomy singular name', 'admin'),
            'search_items' => __('Search Product Categories', 'admin'),
            'popular_items' => __('Popular Product Categories', 'admin'),
            'all_items' => __('All Product Categories', 'admin'),
            'parent_item' => __('Parent Category', 'admin'),
            'parent_item_colon' => __('Parent Category:', 'admin'),
            'edit_item' => __('Edit Product Category', 'admin'),
            'update_item' => __('Update Product Category', 'admin'),
            'add_new_item' => __('Add New Product Category', 'admin'),
            'new_item_name' => __('New Product Category Name', 'admin'),
            'separate_items_with_commas' => __('Separate Product Categories with commas', 'admin'),
            'add_or_remove_items' => __('Add or remove Product Category', 'admin'),
            'choose_from_most_used' => __('Choose from the most used Product Categories', 'admin'),
            'not_found' => __('No Product Categories found.', 'admin'),
            'menu_name' => __('Product Categories', 'admin')
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'has_archive' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            'show_in_rest'      => true,
            'rewrite' => array(
                'slug' => $pageName,
                'with_front'=> true,
            ),
        );

        register_taxonomy('product-category', array('product'), $args);

        //Brand categories
        $labels = array(
            'name' => _x('Brand Categories', 'taxonomy general name', 'admin'),
            'singular_name' => _x('Brand Category', 'taxonomy singular name', 'admin'),
            'search_items' => __('Search Brand Categories', 'admin'),
            'popular_items' => __('Popular Brand Categories', 'admin'),
            'all_items' => __('All Brand Categories', 'admin'),
            'parent_item' => __('Parent Category', 'admin'),
            'parent_item_colon' => __('Parent Category:', 'admin'),
            'edit_item' => __('Edit Brand Category', 'admin'),
            'update_item' => __('Update Brand Category', 'admin'),
            'add_new_item' => __('Add New Brand Category', 'admin'),
            'new_item_name' => __('New Brand Category Name', 'admin'),
            'separate_items_with_commas' => __('Separate Brand Categories with commas', 'admin'),
            'add_or_remove_items' => __('Add or remove Brand Category', 'admin'),
            'choose_from_most_used' => __('Choose from the most used Brand Categories', 'admin'),
            'not_found' => __('No Brand Categories found.', 'admin'),
            'menu_name' => __('Brand Categories', 'admin')
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_in_rest'      => true,
            'has_archive' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
        
        );

        register_taxonomy('brand-category', array('product'), $args);

        add_filter( 'rwmb_meta_boxes', 'product_meta_boxes' );

        function product_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'product-category',
                'title'      => '',
                'taxonomies' => 'product-category',
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
                  
                    array(
                        'id'               => 'product_cat_image',
                        'name'             => 'Icon',
                        'type'             => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status'       => 'false',
                        'image_size'       => 'thumbnail',
                    ),      
                )
            );

            $meta_boxes[] = array(
                'id'         => 'brand-category',
                'title'      => '',
                'taxonomies' => 'brand-category',
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
                  
                    array(
                        'id'               => 'brand_cat_image',
                        'name'             => 'Icon',
                        'type'             => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status'       => 'false',
                        'image_size'       => 'thumbnail',
                    ),      
                )
            );
            return $meta_boxes;
        }

    }

}
new Product();