<?php
add_action('wp_ajax_filterProducts', 'filterProducts');
add_action('wp_ajax_nopriv_filterProducts', 'filterProducts');

function filterProducts(){
	if(isset($_POST['orderBy'])){
        $orderBy = $_POST['orderBy'];
    }else{
        $orderBy = 0;
    }

    $strURL = $_POST['url'];
    $urlVal = explode("/",$strURL);
    $found = 0;
    foreach ($urlVal as $index => $value)
    {
        if($value == 'page') $found = $index;
    }
    $value = $found +1;
    $pageNumber = $urlVal[$value];
    $pageNumber = intval($pageNumber) ? intval($pageNumber) : 1;
    $pages = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : $pageNumber;
	$postsPerPage = 12;

	$brands = $_POST['brands'];
	$brandArr = explode(',', $_POST['brands'][0]);

    $solution = $_POST['solution'];
    $solutionArr = explode(',', $_POST['solution'][0]);

    $cats = $_POST['cat'];
    $catArr = explode(',', $_POST['cat'][0]);

    $page = intval($_POST['page']);

 	$args = array(
        'post_type' => 'product',
        'orderby'=> 'title',
        'order' => 'ASC',
        'posts_per_page'   => $postsPerPage,
        'post_status'      => 'publish',
        'paged'          => $pages,
        
        'meta_query' => array(
            'relation' => 'OR'
           
        )

    );

    $args['tax_query'] = array();

    if(  isset( $_POST['brands'] ) ) {
        $array = array(
            array(
                'taxonomy' => 'brand-category',
                'field' => 'slug',
                'terms' => array_filter($brandArr),
            )
        );
        array_push($args['tax_query'], $array);
    }

    if( $_POST['solution'] ) {
        $array = array(
                'key' => 'solution',
                'value' => $solutionArr[0],
                'compare' => '='
            );

       array_push($args['meta_query'], $array);
    }

    if(  isset( $_POST['cat'] ) ) {
        $array = array(
            array(
                'taxonomy' => 'product-category',
                'field' => 'slug',
                'terms' => array_filter($catArr),
            )
        );
        array_push($args['tax_query'], $array);
    }
    if(  isset( $_POST['term'] ) && $_POST['term'] != 'false') {
        $array = array(
            array(
                'taxonomy' => 'product-category',
                'field' => 'slug',
                'terms' => $_POST['term'],
            )
        );
        array_push($args['tax_query'], $array);
    }
    
	$query = new WP_Query( $args );
  
	$html =  "";
	if( $query->have_posts() ) :
            while( $query->have_posts() ): $query->the_post();
                $html .= "<div class='col-sm-4'>";
                
                    $terms = wp_get_post_terms( get_the_ID(), 'product-category');
                    $html .= "<a href='" . get_the_permalink( )  . "'>";
                    $html .= '<div class="product-item">';
                    // $html .= '<div class="product-item__term">';
                    // if( $terms ) : 
                    //     $html .= '<p>' . $terms[0]->name . '</p>';
                    // endif;
                    // $html .= '</div>';
                    $html .= '<div class="product-item__image" style="background-image:url('. get_the_post_thumbnail_url() .');">';
                    $html .= '</div>';
                    $html .= '<p class="product-item__name">'. get_the_title( ) .'</p>';
                    $html .= '</div>';
                    $html .= '</a>';
                    $html .= "</div> ";

            endwhile;

            wp_reset_postdata();
    else :
        $html .=  '<h3 class="no-result">'.pll__('Nekas netika atrasts', 'general').'</h3>';     
    endif;
    $html .=  ''; // END catalog__list

    $html .=  '<div class="pager">';



        $position = strpos($_POST['url'] , '/page' );
        $nopaging_url = ( $position ) ? substr( $_POST['url'], 0, $position ) : $_POST['url'];
        $pages = paginate_links( array(
            'base' => str_replace('/%_%', '%_%', $nopaging_url.'%_%'.$_POST['search']),
            'format' => '/page/%#%/',
            'current' => $pageNumber,
            'total' => $query->max_num_pages,
            'prev_text'          => file_get_contents(get_template_directory().'/assets/images/adk-icon-bulta-right-red.svg'),
            'next_text'          => file_get_contents(get_template_directory().'/assets/images/adk-icon-bulta-right-red.svg'),
            'type'  => 'array',
        ) );

        if( $pages ) {

            $html .=  '<ul class="pager__list">';
            foreach ( $pages as $page ) {
                $html .=  '<li class="pager__item">'.$page.'</li>';
            }
            $html .=  '</ul>';
        }

        $html .=  '</div>';
        echo $html;
    die;

}

function ajax_pager($query = null, $paged = 1){

    if (!$query)
        return;

    $big = 999999999; // need an unlikely integer
    $base   = str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) );
    $format = 'page/%_%';

      $pages = paginate_links( array(
            'base' => $base,
            'format' => $format,
            'current' => max( 1, get_query_var('paged') ),
            'total' => $query->max_num_pages,
            'prev_text'          => file_get_contents(get_template_directory().'/assets/images/adk-icon-bulta-right-red.svg'),
            'next_text'          => file_get_contents(get_template_directory().'/assets/images/adk-icon-bulta-right-red.svg'),
            'type'  => 'array',
        ) );

    if ($query->max_num_pages > 1) : ?>
        <ul class="pagination">
            <?php foreach ( $pages as $page ) :?>
                <li><?php echo $page; ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif;
}