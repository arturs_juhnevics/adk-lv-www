<?php

class Solution{

    public function __construct() {

        $labels = array(
            'name'               => "Solutions",
            'singular_name'      => "Solution",
            'menu_name'          => "Solutions",
            'add_new_item'       => "Add Solution",
            'new_item'           => "New Solution",
            'edit_item'          => "Edit Solution",
            'view_item'          => "View Solution",
            'all_items'          => "All Solutions",
            'search_items'       => "Search Solutions",
            'not_found'          => "No Solution found",
            'not_found_in_trash' => "No Solution found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'capabilities' => array(

            ),
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => true,
            'supports' => array('thumbnail', 'editor', 'title'),  
            'rewrite'      => array('slug' => 'risinajumi', 'with_front' => false)  
           
        );

        register_post_type('solution', $args);

        add_filter( 'rwmb_meta_boxes', 'solution_meta_boxes' );
        function solution_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'solution',
                'title'      => 'Extra',
                'post_types' => array('solution'),
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(

                    array(
                        'id'    => 'solution_short_text',
                        'placeholder' => 'Short description',
                        'name' => 'Short Description',
                        'type'  => 'textarea',
                       
                    ),
                        
                )
            );

            $meta_boxes[] = array(
                'id'         => 'solution_products',
                'title'      => 'Products',
                'post_types' => array('solution'),
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(

                   array(
                        'name'        => 'Select a page',
                        'id'          => 'solution_product',
                        'type'        => 'post',

                        'multiple'        => true,
                        // Post type.
                        'post_type'   => 'product',

                        // Field type.
                        'field_type'  => 'select_advanced',

                        // Placeholder, inherited from `select_advanced` field.
                        'placeholder' => 'Select a product',

                        // Query arguments. See https://codex.wordpress.org/Class_Reference/WP_Query
                        'query_args'  => array(
                            'post_status'    => 'publish',
                            'posts_per_page' => - 1,
                        ),
                    ),
               )
            );

            return $meta_boxes;
        }


    }

}
new Solution();