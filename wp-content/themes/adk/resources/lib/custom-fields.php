<?php

function init_custom_fields()
{
    /************************************************
     *              Custom options page             *
     ***********************************************/

    $base = dirname(__FILE__) . '/fields/';
    $fields = array(
        'home' => array(
            'home'
        ),
        'options' => array(
            'options'
        ),
        'contacts' => array(
            'contacts'
        ),
        'catalog' => array(
            'catalog'
        ),
    );

    foreach ($fields as $type => $boxes) {
        foreach ($boxes as $box) {
            $file =  $base . $type . '/' . $box . '.php';
            if (file_exists($file)) {
                include $file;
            }
        }
    }
    
}

if ( function_exists( 'rwmb_meta' ) ) {
    add_action('after_setup_theme', 'init_custom_fields');
}