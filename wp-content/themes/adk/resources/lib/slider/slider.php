<?php

class Slider{

    public function __construct() {

        $labels = array(
            'name'               => "Slider",
            'singular_name'      => "Slider",
            'menu_name'          => "Slider",
            'add_new_item'       => "Add Slider",
            'new_item'           => "New Slider",
            'edit_item'          => "Edit Slider",
            'view_item'          => "View Slider",
            'all_items'          => "All Sliders",
            'search_items'       => "Search Sliders",
            'not_found'          => "No Slider found",
            'not_found_in_trash' => "No Slider found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'capabilities' => array(

            ),
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => false,
            'supports' => array('thumbnail', 'title'),    
           
        );

        register_post_type('slider', $args);

 		add_filter( 'rwmb_meta_boxes', 'slider_meta_boxes' );

        function slider_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'slider',
                'title'      => 'Slide info',
                'post_types' => array('slider'),
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
					array(
                        'id'    => 'slider_title',
                        'placeholder' => 'Title',
                        'name' => 'Title',
                        'type'  => 'text',
                        'size' => 60,
                       
                    ),
                    array(
                        'id'    => 'slider_text',
                        'placeholder' => 'Text',
                        'name' => 'Text',
                        'type'  => 'text',
                        'size' => 60,
                       
                    ),
                    array(
                        'id'    => 'slider_button',
                        'placeholder' => 'Button text',
                        'name' => 'Button text',
                        'type'  => 'text',
                        'size' => 60,
                       
                    ),
                    array(
                        'id'    => 'slider_button_url',
                        'placeholder' => 'Button URL',
                        'name' => 'Button URl',
                        'type'  => 'text',
                        'size' => 60,
                       
                    ),
                        
                )
            );

            return $meta_boxes;
        }
       


    }

}
new Slider();
