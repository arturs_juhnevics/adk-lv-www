<?php

$strings = array(
    'Go to homepage' => 'General',
    "Pieprasīt cenu" => 'General',

    'Saistītie prodokti' => 'Produkti',
    'Filtrs: ' => 'Produkti',
    'Atlasīt preces' => 'Produkti',
    'Atpakaļ' => 'Produkti',
    'Atcelt visus' => 'Produkti',
    'Ražotāji' => 'Produkti',
    'Apakštips' => 'Produkti',
    'Risinājumi' => 'Produkti',

    'Kontaktinformācija' => 'Contacts',
    'Adrese:' => 'Contacts',
    'Darba Laiks:' => 'Contacts',
    'E-pasts:' => 'Contacts',
    'Tālrunis:' => 'Contacts',
    'Rekvizīti' => 'Contacts',
    'Reģistrācijas numurs:' => 'Contacts',
    'PVN Reģistra numurs:' => 'Contacts',
    'Banka:' => 'Contacts',
    'Bankas:' => 'Contacts',
    'Reģistrācijas numurs:' => 'Contacts',
    'Komanda' => 'Contacts',

    'Pieteikt vizīti' => 'Form',
    'Vārds, Uzvārds' => 'Form',
    'Telefons' => 'Form',
    'Uzņēmuma nosaukums' => 'Form',
    'Epasts' => 'Form',
    'Uzņēmuma darbības joma' => 'Form',
    'Vēlamais datums' => 'Form',
    'Ziņas teksts' => 'order-form',
    'Nosūtīt' => 'Form',

    "Produkti" => "Titles",
    "Solutions" => "Titles",

);

if( function_exists('pll_register_string') ) {
    foreach ( $strings as $string => $category ) {
        pll_register_string($string, $string, $category);
    }
}
