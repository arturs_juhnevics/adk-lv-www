<?php
add_filter( 'rwmb_meta_boxes', 'home_meta_boxes' );
function home_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
            'title'      => 'Solutions',
            'post_types' => array('page'),
            'include' => array(
                'relation'        => 'OR',
                'template'        => array('views/template-home.blade.php')
            ),
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(
                
                array(
                    'id'   => 'solutions_url',
                    'name' => 'URL',
                    'type' => 'text',
                    'size' => '60',
                ),
                array(
                    'id'        => 'enable_solutions',
                    'name'      => 'Enable solutions',
                    'type'      => 'switch',
                    
                    // Style: rounded (default) or square
                    'style'     => 'rounded',

                    // On label: can be any HTML
                    'on_label'  => 'Yes',

                    // Off label
                    'off_label' => 'No',
                ),
            ),
        );
    $meta_boxes[] = array(
        'title'      => 'Facts',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-home.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'facts_title',
                'name' => 'Title',
                'type' => 'text',
                'placeholder' => 'Title',
                'size' => '60',
            ),
            array(
                'id'   => 'facts_text',
                'name' => 'Text',
                'type' => 'textarea',
                'placeholder' => 'Text',
                'size' => '60',
            ),
            array(
                'id'   => 'facts_url',
                'name' => 'URL',
                'type' => 'text',
                'size' => '60',
            ),
            array(
                'id'               => 'facts_image',
                'name'             => 'Image',
                'type'             => 'image_advanced',
                'max_file_uploads' => 1,
                'max_status'       => 'false',
                'image_size'       => 'thumbnail',
            ),
            array(
                'id' => 'facts',
                'clone' => true,
                'type'   => 'group',
                'collapsible' => true,
                'group_title' => array( 'field' => 'title' ),
                'fields' => array(
                    array(
                        'id'   => 'number',
                        'name' => 'Number',
                        'type' => 'text',
                        'size' => '60',
                    ),
                    array(
                        'id'   => 'title',
                        'name' => 'Title',
                        'type' => 'text',
                        'size' => '60',
                    ),
                    array(
                        'id'   => 'text',
                        'name' => 'Text',
                        'type' => 'text',
                        'size' => '60',
                    ),
                ),
            ),
            array(
                    'id'        => 'enable_facts',
                    'name'      => 'Enable Facts',
                    'type'      => 'switch',
                    
                    // Style: rounded (default) or square
                    'style'     => 'rounded',

                    // On label: can be any HTML
                    'on_label'  => 'Yes',

                    // Off label
                    'off_label' => 'No',
                ),
        ),
    );
    $meta_boxes[] = array(
            'title'      => 'Partners',
            'post_types' => array('page'),
            'include' => array(
                'relation'        => 'OR',
                'template'        => array('views/template-home.blade.php')
            ),
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(
                array(
                    'id'   => 'Partners_title',
                    'name' => 'Title',
                    'type' => 'text',
                    'placeholder' => 'Title',
                    'size' => '60',
                ),
                array(
                    'id'   => 'Partners_text',
                    'name' => 'Text',
                    'type' => 'textarea',
                    'placeholder' => 'Text',
                    'size' => '60',
                ),
                array(
                    'id' => 'partenrs',
                    'clone' => true,
                    'type'   => 'group',
                    'collapsible' => true,
                    'group_title' => array( 'field' => 'partners_title' ),
                    'fields' => array(

                        array(
                            'id'   => 'partners_title',
                            'name' => 'Title',
                            'type' => 'text',
                            'size' => '60',
                        ),
                       array(
                            'id'               => 'partners_image',
                            'name'             => 'Image',
                            'type'             => 'image_advanced',
                            'max_file_uploads' => 1,
                            'max_status'       => 'false',
                            'image_size'       => 'thumbnail',
                        ),
                    ),
                ),
            ),
        );
    return $meta_boxes;
}