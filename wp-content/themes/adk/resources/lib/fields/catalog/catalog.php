<?php
add_filter( 'rwmb_meta_boxes', 'catalog_meta_boxes' );
function catalog_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'id'         => 'catalog',
        'title'      => 'Catalog',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-catalog.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(

             array(
                'id'               => 'product_files',
                'name'             => 'Downloadable files',
                'type'             => 'file_upload',
                'max_status'       => 'false',
                'image_size'       => 'thumbnail',
            ), 
        )
    );
    return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'media_attachments' );
function media_attachments( $meta_boxes ) {
    $meta_boxes[] = array(
        'id'         => 'media-attach',
        'title'      => 'Preview',
        'post_types' => array('attachment'),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(

             array(
                'id'               => 'preview_image',
                'name'             => 'Preview Image',
                'type'             => 'image_advanced',
                'image_size'       => 'thumbnail',
            ), 
        )
    );
    return $meta_boxes;
}