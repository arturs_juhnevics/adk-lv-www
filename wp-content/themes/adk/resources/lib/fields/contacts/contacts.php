<?php
add_filter( 'rwmb_meta_boxes', 'contacts_meta_boxes' );
function contacts_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => 'General',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-contacts.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
             array(
                'id' => 'general',
                'clone' => false,
                'type'   => 'group',
                'collapsible' => true,
                'group_title' => "General info",
                'fields' => array(
                    array(
                        'id'   => 'contacts_address',
                        'name' => 'Address',
                        'type' => 'text',
                        'size' => '60',
                    ),            
                    array(
                        'id'   => 'contacts_email',
                        'name' => 'Email',
                        'type' => 'text',
                        'size' => '60',
                    ),
                    array(
                        'id'   => 'contacts_open',
                        'name' => 'Opening times',
                        'type' => 'text',
                        'size' => '60',
                    ),
                    array(
                        'id'   => 'contacts_phone',
                        'name' => 'Phone',
                        'type' => 'text',
                        'size' => '60',
                    ),
                )
            ),
            array(
                'id' => 'company',
                'clone' => false,
                'type'   => 'group',
                'collapsible' => true,
                'group_title' => "Company details",
                'fields' => array(
                    array(
                        'id'   => 'reg_number',
                        'name' => 'Reg. number',
                        'type' => 'text',
                        'size' => '60',
                    ),            
                    array(
                        'id'   => 'pvn_number',
                        'name' => 'PVN number',
                        'type' => 'text',
                        'size' => '60',
                    ),
                    array(
                        'id'   => 'bank',
                        'name' => 'Bank',
                        'type' => 'text',
                        'size' => '60',
                    ),
                    array(
                        'id'   => 'bank_code',
                        'name' => 'Bank code',
                        'type' => 'text',
                        'size' => '60',
                    ),
                    array(
                        'id'   => 'bank_acc_number',
                        'name' => 'Bank account number',
                        'type' => 'text',
                        'size' => '60',
                    ),
                )
            ),
        ),
    );
        
    $meta_boxes[] = array(
        'title'      => 'Team',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-contacts.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
             array(
                'id' => 'team',
                'clone' => true,
                'type'   => 'group',
                'collapsible' => true,
                'sort_clone' => true,
                'group_title' => array( 'field' => 'name' ),
                'fields' => array(
                    array(
                        'id'               => 'image',
                        'name'             => 'Happy face',
                        'type'             => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status'       => 'false',
                        'image_size'       => 'thumbnail',
                    ),array(
                        'id'               => 'image_sad',
                        'name'             => 'Sad face',
                        'type'             => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status'       => 'false',
                        'image_size'       => 'thumbnail',
                    ),
                    array(
                        'id'   => 'position',
                        'name' => 'Position',
                        'type' => 'text',
                        'size' => '60',
                    ),
                    array(
                        'id'   => 'name',
                        'name' => 'Name',
                        'type' => 'text',
                        'size' => '60',
                    ),
                    array(
                        'id'   => 'phone',
                        'name' => 'Phone',
                        'type' => 'text',
                        'size' => '60',
                    ),
                    array(
                        'id'   => 'email',
                        'name' => 'Email',
                        'type' => 'text',
                        'size' => '60',
                    ),
                ),
            ),

        )
    );
   

    return $meta_boxes;
}