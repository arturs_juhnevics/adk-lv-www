var common = {
    init: function () {
      common.scroll.init();
      common.events.init();
     $('.hero').on('init', function() {
          $('.hero .slick-dots').wrap('<div class="hero__dots dots"></div>');
        }),
      $('.hero').slick({
         slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false,
                fade: false,
                autoplay: true,
                dots: true,
                arrows: false,
                autoplaySpeed: 5000,
                adaptiveHeight: false,
                draggable: true,
       });

      $('.solutions__slider').on('init', function() {
        $('solutions__slider .slick-dots').wrap('<div class="solutions__dots dots"></div>');
      });
     

        var $status = $('.slider_paging');
      var $slickElement = $('.solutions__slider');

      $slickElement.on('init reInit afterChange', function (event, slick, currentSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $status.empty();
        $status.append('<span class="slider_paging__first">' + i + '</span>' + '<span class="slider_paging__last"> / ' + slick.slideCount + '</span>');
      });

       $slickElement.slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          fade: false,
          autoplay: false,
          dots: false,
          arrows: true,
          autoplaySpeed: 5000,
          adaptiveHeight: false,
          draggable: true,
          nextArrow: '<div class="slick-next slick-arrow"></div>',
          prevArrow: '<div class="slick-prev slick-arrow"></div>',
       });
    },
    events: {
      init: function () {

        /* Mobile hamburger */
        $('.hamburger').click( function (e) {
          e.stopImmediatePropagation();
          $(this).toggleClass('is-active');
          $('header').toggleClass('menu-open');
          $('html').toggleClass('no-scroll');
        });

        /* Expand text about us */
        var readmore = false;
        var readmoreText = $('.aboutus-readmore').html();
        var readlessText = $('.aboutus-readmore').attr('data-alt-text');
        $('.aboutus-readmore').unbind('click').click(function(){
          $('.about_us__text').toggleClass('expand');
          if(readmore == false){
            $('.aboutus-readmore').html(readlessText);
            readmore = true;
          }else{
            $('.aboutus-readmore').html(readmoreText);
            readmore = false;
          }
          
        });

        $('.button-close').click( function (e) {
          e.preventDefault();
          $('.newsletter').removeClass('animate');
          $('.newsletter').removeClass('delay4');
          $('.newsletter').removeClass('animate--visible');
          common.setCookie('show_newsletter','no');
        });

        $('#newsletter-form').unbind('submit').submit(function(e){
          e.preventDefault();
          $.ajax({
                    url        : ajaxurl,
                    type       : 'POST',
                    data       : {
                        'action'  : 'submit_newsletter',
                        'email'   : $('#newsletter-email').val(),
                        'listid'  : $('input[name=sender-list]:checked').val(),
                        'contact-nonce'      : $('#contact-nonce').val(),
                        'contact-honey'      : $('#contact-honey').val(),

                    },
                    beforeSend : function() {
                        $('.form__status').addClass('hidden');
                        $('#newsletter-form .button').addClass('loading');
                    },
                })
                    .done(function (response) {
                      var parsedResponse = $.parseJSON(response.data);
                        console.log(parsedResponse);
                        $('#contact-form button, .contacts__form').removeClass('loading');
                        if (parsedResponse.data.saved == 1) {
                   
                            $('.newsletter__status').removeClass('error').text(translations.newsletter_added).removeClass('hidden');
                            $('#newsletter-form').empty();
                            $('.newsletter').removeClass('animate');
                            $('.newsletter').removeClass('animate--visible');
                            common.setCookie('show_newsletter','no');
                        
                        }
                        else{
                            $('.newsletter__status').addClass('error').text(translations.newsletter_error).removeClass('hidden');
                        }
                    }); 
        });
      }

      
    },

    scroll: {

        sticky: false,
        topbar: 0,
        last: 0,

        init: function () {
            common.scroll.do();
            
            $(window).unbind('scroll').scroll(function () {
                common.scroll.do();
             
            });
        },

        do: function () {

            var st = $(window).scrollTop();
            if($('body').hasClass('admin-bar')){
              st = $(window).scrollTop() + 32;
              common.scroll.topbar = 32;
            }
          
            if (st >= $('header').offset().top && st !== common.scroll.topbar) {
            
                common.scroll.sticky = true;
                $('header').addClass('header--scrolled');

            } else if (st == common.scroll.topbar) {
        
                common.scroll.sticky = false;
                $('header').removeClass('header--scrolled');
            }

            common.scroll.last = st;
            common.checkVisibleElements();
        }
      },

      setCookie: function(cookie_name, value)
      {
          var exdate = new Date();
          exdate.setDate(exdate.getDate() + (30));
          document.cookie = cookie_name + "=" + escape(value) + "; expires="+exdate.toUTCString() + "; path=/";
      },

      getCookie: function(cookie_name)
      {
          if (document.cookie.length>0)
          {
              cookie_start = document.cookie.indexOf(cookie_name + "=");
              if (cookie_start != -1)
              {
                  cookie_start = cookie_start + cookie_name.length+1;
                  cookie_end = document.cookie.indexOf(";",cookie_start);
                  if (cookie_end == -1)
                  {
                      cookie_end = document.cookie.length;
                  }
                  return unescape(document.cookie.substring(cookie_start,cookie_end));
              }
          }
          return "";
      },

      checkVisibleElements: function(elements){
            var windowScrolled = $(window).scrollTop() + $(window).height();
            var index = 0;
  
            $('.animate:not(.animate--visible, .animate--waiting)').each(function(i, element){
                if($(element).offset().top < windowScrolled){
                    setTimeout(function(){
                        $(element).addClass('animate--visible').removeClass('animate--waiting');
                    }, $(element).index() * 100 );
                    $(element).addClass('animate--waiting');
                }
            });
        },

}
common.init();