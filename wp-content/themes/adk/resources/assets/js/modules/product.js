var product = {
    init: function () {

      $('td[rowspan]:even').addClass('oddrow');
      
      var $slickElement = $('.gallery__main__inner');
      var $thumbs = $('.gallery__thumbs__inner');
      var items = $('.gallery__main__item').length;

      $('.gallery__main__inner').not('.slick-initialized').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            fade: false,
            autoplay: true,
            dots: false,
            arrows: true,
            autoplaySpeed: 5000,
            adaptiveHeight: false,
            draggable: true,
            nextArrow: '<div class="slick-next slick-arrow"></div>',
            prevArrow: '<div class="slick-prev slick-arrow"></div>',
       });

        // $thumbs.slick({
        //     slidesToShow: 5,
        //     slidesToScroll: 1,
        //     asNavFor: $slickElement,
        //     dots: false,
        //     arrows: false,
        //     focusOnSelect: true,
        //     centerMode: false,
        //     infinite: items > 3,
        //     centerPadding: 10,
        //     responsive: [
        //         {
        //             breakpoint: 992,
        //             settings: {
        //                 slidesToShow: 3,
        //                 infinite: items > 5,
        //             },
        //         },
        //         {
        //             breakpoint: 500,
        //             settings: {
        //                 slidesToShow: 3,
        //                 infinite: items > 3,
        //             },
        //         },
        //     ],
        // });

        $('.related-posts .post-slider').not('.slick-initialized').slick({
         slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          fade: false,
          autoplay: false,
          arrows: true,
          adaptiveHeight: false,
          draggable: true,
          centerMode: false,
          prevArrow: $('.related-prev'),
          nextArrow: $('.related-next'),
          responsive: [
              {
                  breakpoint: 992,
                  settings: {
                    slidesToShow: 1,
                  }
              }
          ]
       });

    },
    
}
product.init();