var filter = {
    init: function () {
      filter.events();
      filter.checkFilter();
     
      var url_string = window.location.href;
      
      function detectQueryString() {
          // get the current URL
          var currentUrl = window.location.href;

          // regex pattern for detecting querystring
          var pattern = new RegExp(/\?.+=.*/g);

          return pattern.test(currentUrl);
      }
      if (detectQueryString()) {
        
      }

      $('.filter-button').on('click', function(){
        $(".product-filter").removeClass('closed');
      });
      $('.filter-back').on('click', function(){
        $(".product-filter").addClass('closed');
      });
    },

    data: {
        selectedValues : {"brand":"","solutions":"", "cat":""},
        page: "",
    },

    checkFilter: function() {
        var url_string = window.location.href;
        var url = new URL(url_string);

        var brands    = url.searchParams.getAll("brand");
        var solutions = url.searchParams.getAll("solutions");
        var cat = url.searchParams.getAll("cat");

        filter.data.selectedValues['brand'] = brands;
        filter.data.selectedValues['solutions'] = solutions;
        filter.data.selectedValues['cat'] = cat;

        if(solutions != ""){
          var brandArr = brands[0].split(',');
          $.each( brandArr, function( index, value ){
             if($('input[value="'+value+'"]')){
                $('input[value="'+value+'"]').prop( "checked", true );
              }
          });
        }

        if(solutions != ""){
           var solutionsArr = solutions[0].split(',');
           $.each( solutionsArr, function( index, value ){
              if($('input[value="'+value+'"]')){
                $('input[value="'+value+'"]').prop( "checked", true );
              }
           });
        }

        if(cat != ""){
           var catArr = cat[0].split(',');
           $.each( catArr, function( index, value ){
              if($('input[value="'+value+'"]')){
                $('input[value="'+value+'"]').prop( "checked", true );
              }
           });
        }
        
    },

    events: function () {

      $("input[type='checkbox'], input[type='radio'], .pagination a").on('click', function(){

        var brand_string = "";
        var solution_string = "";
        var cat_string = "";

        filter.data.selectedValues['brand'] = "";
        filter.data.selectedValues['solutions'] = "";
        filter.data.selectedValues['cat'] = "";
        //console.log(filter.data.selectedValues);
        $("input[name='brand']").each(function(){
          if (this.checked) {
            brand_string += $(this).val() + ',';
          }
        });

        $("input[name='solutions']").each(function(){
          if (this.checked) {
            solution_string += $(this).val() + ',';
          }
        });

        $("input[name='category']").each(function(){
          if (this.checked) {
            cat_string += $(this).val() + ',';
          }
        });

        if( $(this).data('filter') ){
          filter.data.page = $(this).data('page');
        }else{
          filter.data.page = parseInt($(this).attr('href').replace(/\D/g,''));
        }

        filter.data.selectedValues['brand'] = brand_string;
        filter.data.selectedValues['solutions'] = solution_string;
        filter.data.selectedValues['cat'] = cat_string;
        filter.generateUrl();
        filter.filterProducts();
      });

      $('#cancel-all').on('click', function(){
        filter.data.selectedValues['brand'] = "";
        filter.data.selectedValues['solutions'] = "";
        filter.data.selectedValues['cat'] = "";

        $('input').prop( "checked", false );
        filter.generateUrl();
        filter.filterProducts();
      });      

    },

    generateUrl: function () {
        var currentUrl = window.location.href.split(/[?#]/)[0];
        currentUrl += '?';
        emptyUrl = currentUrl.length;
        for(var key in filter.data.selectedValues){
            if(filter.data.selectedValues[key].length > 0){
              var values = filter.data.selectedValues[key];
              currentUrl += '&'+key+'='+values;
            }
            
        }

      window.history.pushState("", "Filter", currentUrl);
      filter.filterProducts();
    },

    filterProducts: function(){

      if ( window.location.pathname != '/' ) {
        var url_string = window.location.href;
        var url = new URL(url_string);

        var brands    = url.searchParams.getAll("brand");
        var solutions = url.searchParams.getAll("solutions");
        var cat = url.searchParams.getAll("cat");

        // Append search values to array
        if(url_string){
          filter.data.selectedValues['brand'] = brands;
          filter.data.selectedValues['solutions'] = solutions;
          filter.data.selectedValues['cat'] = cat;
        }

        // Ajax call to filter

        $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {
                    brands: brands,
                    solution: solutions,
                    cat: cat,
                    action: 'filterProducts',
                    url: window.location.pathname,
                    search: String(window.location.search),
                    tax: $("#curTax").val(),
                    term: $("#curTerm").val(),
                },

                beforeSend: function (xhr) {
                    
                    $('.product-container').addClass('loading');
                   
                },

                success: function (data) {
                    $('.product-list').empty();
                    $('.product-list').html(data) // insert data
                    $('.product-container').removeClass('loading');
                    //console.log(data);
                    
                }
            });
        
      };

    },
}
filter.init();