requirejs.config({
    baseUrl: base_url + '/assets/js/modules',
    urlArgs: "v=1.0.6",
    waitSeconds : 30,
    paths: {

        // plugins
        'async'             : '../plugins/async',
        'slick'             : '../plugins/slick.min',
        'jquery'            : '../plugins/jquery3.3.1.min',
        'selectize'         : '../plugins/selectize.min',
        'bootstrap'         : '../plugins/bootstrap.bundle.min',
        'jqueryui'         : '../plugins/jqueryui-1.12.1',


        // modules
        'common'                            : 'min/common',
        'filter'                            : 'min/filter',
        'page_template_template_contacts'   : 'min/contacts',
        'single_product'                    : 'min/product',
        //'tax_product_category'              : 'min/filter',


    },

    shim: {
        
        'common': {
            deps: ['async', 'jquery', 'slick', 'bootstrap', 'filter']
        },
        'page_template_template_contacts' : {
            deps : ['async','jqueryui']
        },
        'filter': {
            deps: ['async']
        },
        'single_product': {
            deps: ['async']
        },
    }
});

requirejs(['common'], function() {
    (function($) {
        var UTIL = {
            loadEvents: function() {
                common.init();
                $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                    if (typeof(requirejs.s.contexts._.config.paths[classnm]) != 'undefined') {
                        requirejs([classnm], function() {

                            if (typeof(this[classnm]) !== 'undefined') {
                                if (typeof(this[classnm].finalize) !== 'undefined') {
                                    this[classnm].finalize();
                                }
                            }
                        });
                    }
                });
            }
        };
        // Load Events
        $(document).ready(UTIL.loadEvents);
    })(jQuery); // Fully reference jQuery after this point.
});