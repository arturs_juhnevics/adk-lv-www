requirejs.config({
    baseUrl: base_url + '/assets/scripts/modules',
    urlArgs: "v=1.0.3",
    waitSeconds : 30,
    paths: {

        // plugins
        'async'          : '../plugins/async',
        'slick'          : '../plugins/slick.min',
        'jquery'      : '../plugins/jquery3.3.1.min',



        // modules
        'common'                         : 'common',
        'page_template_template_contacts': 'contacts',
        'single_product'                 : 'product',

    },

    shim: {
        
        'common': {
            deps: ['jquery', 'slick']
        },
        'page_template_template_contacts' : {
            deps : ['async']
        },

    }
});

requirejs(['common'], function() {
    (function($) {
        var UTIL = {
            loadEvents: function() {
                common.init();
                $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                    if (typeof(requirejs.s.contexts._.config.paths[classnm]) != 'undefined') {
                        requirejs([classnm], function() {

                            if (typeof(this[classnm]) !== 'undefined') {
                                if (typeof(this[classnm].finalize) !== 'undefined') {
                                    this[classnm].finalize();
                                }
                            }
                        });
                    }
                });
            }
        };
        // Load Events
        $(document).ready(UTIL.loadEvents);
    })(jQuery); // Fully reference jQuery after this point.
});