
var contacts = {
    init: function () {
    	
            require(['async!https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyBY0YybMhHoxWjrpXYTK1eC2_SJp9UfFMg'], function() {
            	
                contacts.maps( $('#map-wrap') );
            });
    
   
        $('#contact-form').on('submit', function(e){
            e.preventDefault();
             $.ajax({
                    url        :'http://adk.minimo.lv/wp-admin/admin-ajax.php',
                    type       : 'POST',
                    data       : {
                        'action'  : 'submit_form',
                        'name'       : $('#name').val(),
                        'phone'      : $('#phone').val(),
                        'email'      : $('#email').val(),
                        'company'            : $('#company').val(),
                        'companyType'    : $('#companyType').val(),
                         'date'    : $('#date').val(),
                          'message'    : $('#message').val(),
                        'contact-nonce'      : $('#contact-nonce').val(),
                        'contact-honey'      : $('#contact-honey').val(),
                        'post-id'            : $('#post_id').val(),
                    },
                    beforeSend : function() {
                        $('.form__status').addClass('hidden');
                        $('#contact-form button, .contacts__form').addClass('loading');
                    },
                })
                    .done(function (response) {
                        $('#contact-form button, .contacts__form').removeClass('loading');
                        if (response.success) {
                            $('.errors-wrap').remove();
                            $('.form__status').removeClass('error').text(response.data.message).removeClass('hidden');
                            $('#contact-form')[0].reset();
                           
                            $('#contact-form').find('input, textarea').blur();
                        }
                        else{
                            $('.form__status').addClass('error').text(response.data.message).removeClass('hidden');
                        }
                    }); 
        })
       
    },
    maps: function(elem) {
    	 this.elem = elem;
            var self = this;
            var mapDiv = this.elem[0];

            if ( $(mapDiv).data('zoom') ) {
                self.zoom = parseInt($(mapDiv).data('zoom'));
            }

            if( $(mapDiv).data('pin') ) {
                self.pin = $(mapDiv).data('pin');
            }

            if( $(mapDiv).data('pin2') ) {
                self.pin2 = $(mapDiv).data('pin2');
            }

            var map = new google.maps.Map(mapDiv, {
                center: {lat: $(mapDiv).data('lat'), lng: $(mapDiv).data('lng')},
                zoom: self.zoom,
                disableDefaultUI: false,
                mapTypeControl: false,
                scrollwheel: false,
                draggable: true,
                styles: [ { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#e9e9e9" }, { "lightness": 17 } ] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [ { "color": "#f5f5f5" }, { "lightness": 20 } ] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [ { "color": "#ffffff" }, { "lightness": 17 } ] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [ { "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 } ] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [ { "color": "#ffffff" }, { "lightness": 18 } ] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [ { "color": "#ffffff" }, { "lightness": 16 } ] }, { "featureType": "poi", "elementType": "geometry", "stylers": [ { "color": "#f5f5f5" }, { "lightness": 21 } ] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [ { "color": "#dedede" }, { "lightness": 21 } ] }, { "elementType": "labels.text.stroke", "stylers": [ { "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 } ] }, { "elementType": "labels.text.fill", "stylers": [ { "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 } ] }, { "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "featureType": "transit", "elementType": "geometry", "stylers": [ { "color": "#f2f2f2" }, { "lightness": 19 } ] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [ { "color": "#fefefe" }, { "lightness": 20 } ] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [ { "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 } ] }]
            });

            var bounds = new google.maps.LatLngBounds();

            var center = new google.maps.LatLng($(mapDiv).data('lat'), $(mapDiv).data('lng'));
            var marker_options = {
                position: center,
                map: map,
                animation: google.maps.Animation.DROP
            }
            if( self.pin ) {
                marker_options.icon = self.pin;
            }
            var marker = new google.maps.Marker(marker_options);
            bounds.extend(center);

            var center = new google.maps.LatLng($(mapDiv).data('lat2'), $(mapDiv).data('lng2'));
            var marker_options = {
                position: center,
                map: map,
                animation: google.maps.Animation.DROP
            }
            if( self.pin2 ) {
                marker_options.icon = self.pin2;
            }
            var marker2 = new google.maps.Marker(marker_options);
            bounds.extend(center);

            map.panToBounds(bounds);
    }



}
contacts.init();