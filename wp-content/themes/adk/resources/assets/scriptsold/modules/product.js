var product = {
    init: function () {
      var $slickElement = $('.gallery__main__inner');
      var $thumbs = $('.gallery__thumbs__inner');
      var items = $('.gallery__main__item').length;

       $slickElement.slick({
             slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            fade: false,
            autoplay: false,
            asNavFor: $thumbs,
            dots: false,
            arrows: true,
            autoplaySpeed: 5000,
            adaptiveHeight: false,
            draggable: true,
            nextArrow: '<div class="slick-next slick-arrow"></div>',
            prevArrow: '<div class="slick-prev slick-arrow"></div>',
       });

        $thumbs.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: $slickElement,
            dots: false,
            arrows: false,
            focusOnSelect: true,
            centerMode: true,
            infinite: items > 5,
            centerPadding: 0,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 5,
                        infinite: items > 5,
                    },
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 3,
                        infinite: items > 3,
                    },
                },
            ],
        });
    },
}
product.init();