export default {
  init() {
    // scripts here run on the DOM load event
  },
  finalize() {
    // scripts here fire after init() runs
  },
};