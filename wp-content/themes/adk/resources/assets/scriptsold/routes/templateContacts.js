
const google = window.google;
export default {
  init() {
    // scripts here run on the DOM load event
   console.log('HELLO c');
        $('#contact-form').on('submit', function(e){
            e.preventDefault();
             $.ajax({
                    url        :'http://adk.minimo.lv/wp-admin/admin-ajax.php',
                    type       : 'POST',
                    data       : {
                        'action'  : 'submit_form',
                        'name'       : $('#name').val(),
                        'phone'      : $('#phone').val(),
                        'email'      : $('#email').val(),
                        'company'            : $('#company').val(),
                        'companyType'    : $('#companyType').val(),
                         'date'    : $('#date').val(),
                          'message'    : $('#message').val(),
                        'contact-nonce'      : $('#contact-nonce').val(),
                        'contact-honey'      : $('#contact-honey').val(),
                        'post-id'            : $('#post_id').val(),
                    },
                    beforeSend : function() {
                        $('.form__status').addClass('hidden');
                        $('#contact-form button, .contacts__form').addClass('loading');
                    },
                })
                    .done(function (response) {
                        $('#contact-form button, .contacts__form').removeClass('loading');
                        if (response.success) {
                            $('.errors-wrap').remove();
                            $('.form__status').removeClass('error').text(response.data.message).removeClass('hidden');
                            $('#contact-form')[0].reset();
                           
                            $('#contact-form').find('input, textarea').blur();
                        }
                        else{
                            $('.form__status').addClass('error').text(response.data.message).removeClass('hidden');
                        }
                    }); 
        })
        // JavaScript to be fired on the about us page
        console.log('design centres page');

        let myLatLng = {lat: 1, lng: 1};
        console.log(myLatLng);

        let map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: myLatLng,
        });

        console.log(map);
  },
  finalize() {
    // scripts here fire after init() runs
  },
};
