 $(document).ready(function () {

     $('.hero').on('init', function() {
      $('.hero .slick-dots').wrap('<div class="hero__dots dots"></div>');
    }),
  $('.hero').slick({
     slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            fade: false,
            autoplay: true,
            dots: true,
            arrows: false,
            autoplaySpeed: 5000,
            adaptiveHeight: false,
            draggable: true,
   });

  $('.solutions__slider').on('init', function() {
    $('solutions__slider .slick-dots').wrap('<div class="solutions__dots dots"></div>');
  });
 

    var $status = $('.slider_paging');
  var $slickElement = $('.solutions__slider');

  $slickElement.on('init reInit afterChange', function (event, slick, currentSlide) {
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    var i = (currentSlide ? currentSlide : 0) + 1;
    $status.empty();
    $status.append('<span class="slider_paging__first">' + i + '</span>' + '<span class="slider_paging__last"> / ' + slick.slideCount + '</span>');
  });

   $slickElement.slick({
     slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            fade: false,
            autoplay: false,
            dots: false,
            arrows: true,
            autoplaySpeed: 5000,
            adaptiveHeight: false,
            draggable: true,
            nextArrow: '<div class="slick-next slick-arrow"></div>',
            prevArrow: '<div class="slick-prev slick-arrow"></div>',
   });
 })
