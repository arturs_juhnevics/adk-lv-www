<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

$roots_includes = array(
    'lib/translations.php', // Option fields
    'lib/custom-fields.php', // Option fields
    'lib/page-title.php', // Page title
    'lib/solutions/solutions.php', // Solutions
    'lib/slider/slider.php', // Slider
    'lib/products/product.php', // Products
    'lib/products/product-search.php', // Product search
    'lib/breadcrumbs.php', // Products
    'lib/extras.php', // Extras, Widget area's,
    'lib/Df_Forms.php',     // Forms
);

foreach ($roots_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'roots'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);

add_action('wp_AJAX_svg_get_attachment_url', 'get_attachment_url_media_library');

function get_attachment_url_media_library(){

    $url = '';
    $attachmentID = isset($_REQUEST['attachmentID']) ? $_REQUEST['attachmentID'] : '';
    if($attachmentID){
        $url = wp_get_attachment_url($attachmentID);
    }

    echo $url;

    die();
}

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);

add_action('wp_ajax_submit_form', 'submit_form');
add_action('wp_ajax_nopriv_submit_form', 'submit_form');

 function submit_form()
    {
        if (isset($_POST['contact-nonce']) && wp_verify_nonce($_POST['contact-nonce'],
                'contact-nonce') && $_POST['contact-honey'] === ''
        ) {

            add_filter('wp_mail_content_type', function () {
                return 'text/html';
            });

            $data = array(
                'name'    => strip_tags($_POST['name']),
                'phone'    => strip_tags($_POST['phone']),
                'email'   => strip_tags($_POST['email']),
                'company'   => strip_tags($_POST['company']),
                'companyType'   => strip_tags($_POST['companyType']),
                'date'   => strip_tags($_POST['date']),
                'message' => strip_tags($_POST['message']),
                'id' => strip_tags($_POST['post-id']),
            );

            $output = '<h3>Jauna ziņa no '.get_bloginfo("name").' kontaktformas</h3>';
            $output .= 'Vārds: '    . $data['name']    . '<br />';
            $output .= 'Epasts: '   . $data['email']   . '<br />';
            $output .= 'Telefons: '   . $data['phone']   . '<br />';
            $output .= 'Uzņēmums: '    . $data['company']    . '<br />';
            $output .= 'Darbības joma: '    . $data['companyType']    . '<br />';
            $output .= 'Vēlamais datums: '    . $data['date']    . '<br />';
            $output .= 'Ziņa: '     . $data['message'];

           

         
                foreach ($data as $key => $value) {
                    $data[$key] = json_encode($value);
                }
                $df_form = new Df_Forms();
                $df_form->insert($data, 'Contact form');

               

                $send_to = strip_tags($_POST['send_to']);
                $headers = 'From: '.get_bloginfo("name").' <'.$send_to.'>' . "\r\n" ;
                $sent = wp_mail($send_to, __('Ziņa no '.get_bloginfo("name").' kontaktformas'), $output, $headers);

                $response = array(
                    'message' => pll__('Form submitted successfully')
                );
                wp_send_json_success($response);

                
           
        }

        die;
    }

    
function my_js_variables()
{
    ?>
    <script>
        var ajaxurl = <?php echo json_encode( admin_url( "admin-ajax.php" ) ); ?>,
            base_url = <?php echo json_encode( get_template_directory_uri() ); ?>,
            template_url = <?php echo json_encode( get_stylesheet_directory_uri() ); ?>,
            translations = {
                required    : '<?php echo pll__("Please fill all required fields", "Error Messages") ?>',
                valid_email : '<?php echo pll__("Please provide a valid email address", "Error Messages") ?>',
                valid_captcha : '<?php echo pll__("Please check the reCAPTCHA field", "Error Messages") ?>',
                select : '<?php echo pll__("Choose") ?>',
                prev: '<?php echo pll__("Previous") ?>',
                next : '<?php echo pll__("Next") ?>',
                readmore : '<?php echo pll__("Lasīt vairāk") ?>',
                terms: '<?php echo pll__("You need to agree to Terms & conditions", "Error Messages") ?>'
            },
            gmap_api_key = "AIzaSyCXCYNdBKHz7wJEJO0KHVkqr-MsSDBezk8";
    </script><?php
}
if(function_exists('pll__')) add_action('wp_print_scripts', __NAMESPACE__ . '\\my_js_variables');

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

function fix_svg_thumb_display()
{
    echo '<style>
    td.media-icon img[src$=".svg"], .image img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
      max-width: 100px !important; 
      height: auto !important; 
    }
  </style>';
}

add_action('admin_head', 'fix_svg_thumb_display');

/**
 * Hide all notices
 */
function hide_wp_notice() {
    remove_all_actions( 'admin_notices' );
}
add_action( 'admin_head', 'hide_wp_notice', 1 );    

global $language_codes;

$language_codes = array( // sample lang data
  'italiano' => 'it'
  ,'english' => 'en'
  ,'espanol' => 'es'
  ,'latvian' => 'lv'
  ,'russian' => 'ru'
);

add_action('pmxi_saved_post', 'set_imports_lang', 10, 2);

// using undocumented param $data
// action firm is the following (in fact, it passes 3 params): do_action( 'pmxi_saved_post', $pid, $rootNodes[$i], $is_update );
function set_imports_lang($post_id, $data){
  global $language_codes;
  // 'lingue' is italian, I guess it will be 'languages' in english: it's one of the 2 fields we saved before
  pll_set_post_language($post_id, $language_codes[sanitize_title($data->lingue)]);
}

//Add featured image column
add_filter('manage_product_posts_columns', 'add_featured_image_column');
function add_featured_image_column($defaults) {
    $defaults['featured_image'] = 'Featured Image';
    return $defaults;
}

add_action('manage_product_posts_custom_column', 'show_featured_image_column', 10, 2); 
function show_featured_image_column($column_name, $post_id) {
    if ($column_name == 'featured_image') {
        echo get_the_post_thumbnail($post_id, array(80, 80)); 
    }
}


// Move featured image column to front of row
add_filter('manage_posts_columns', 'thumbnail_column');
function thumbnail_column($columns) {
    $new = array();
    foreach($columns as $key => $title) {
        if ($key=='title') // Put the Thumbnail column before the Author column
            $new['featured_image'] = 'Featured Image';
        $new[$key] = $title;
    }
    return $new;
}
add_action('admin_head', 'my_column_width');

function my_column_width() {
    echo '<style type="text/css">';
    echo '#featured_image { width:120px !important; overflow:hidden }';
    echo '</style>';
}

// Add order by product categories dropdown
add_action( 'restrict_manage_posts', 'filter_products_by_taxonomies' , 10, 2);
function filter_products_by_taxonomies( $post_type, $which ) {

    // Apply this only on a specific post type
    if ( 'product' !== $post_type )
        return;

    // A list of taxonomy slugs to filter by
    $taxonomies = array( 'product-category');

    foreach ( $taxonomies as $taxonomy_slug ) {

        // Retrieve taxonomy data
        $taxonomy_obj = get_taxonomy( $taxonomy_slug );
        $taxonomy_name = $taxonomy_obj->labels->name;

        // Retrieve taxonomy terms
        $terms = get_terms( $taxonomy_slug );

        // Display filter HTML
        echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
        echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
        foreach ( $terms as $term ) {
            printf(
                '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                $term->slug,
                ( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
                $term->name,
                $term->count
            );
        }
        echo '</select>';
    }
}

// remove Yoast seo dropdown filter
add_action( 'admin_init', 'bb_remove_yoast_seo_admin_filters', 20 );
function bb_remove_yoast_seo_admin_filters() {
    global $wpseo_meta_columns ;
    if ( $wpseo_meta_columns  ) {
        remove_action( 'restrict_manage_posts', array( $wpseo_meta_columns , 'posts_filter_dropdown' ) );
        remove_action( 'restrict_manage_posts', array( $wpseo_meta_columns , 'posts_filter_dropdown_readability' ) );
    }
}

function getTplPageURL($TEMPLATE_NAME){
    $url = null;
    $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => $TEMPLATE_NAME
    ));
    if(isset($pages[0])) {
        $url = get_page_link($pages[0]->ID);
    }
    return $url;
}