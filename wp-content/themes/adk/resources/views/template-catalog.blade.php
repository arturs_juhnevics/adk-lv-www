{{--
  Template Name: Catalog
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header')

@while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
@endwhile

<?php
$downloads = rwmb_meta('product_files');
?>

<?php if( $downloads ) : ?>
    <div class="container">
        <div class="catalog-downloads">
        <?php foreach ($downloads as $value) { ?>
        <?php 
            $ext = pathinfo($value['path'], PATHINFO_EXTENSION); 
            $extname = 'file';
            $fileClass = 'regular';
            if($ext == 'xls'){
                $extname = 'xls';
            }
            if($ext == 'pdf'){
                $extname = 'pdf';
                $fileClass = 'pdf';
            }
        ?>
            
            <a href="<?php echo $value['url']; ?>" target="_blank" class="downloads__item downloads__item-image <?php echo $fileClass; ?>">
                <img class="downloads__item__icon" alt="file <?php echo $value['name']; ?>" src="<?php echo get_template_directory_uri()."/assets/images/ADk-pdf-ikona.png"; ?>">
                <p class="downloads__item__name-image"><?php echo $value['name']; ?></p>
                <p class="downloads__item__size-image">1.2 MB</p>
            </a>
        <?php } ?>
        </div>
    </div?
  <?php endif; ?>

@endsection