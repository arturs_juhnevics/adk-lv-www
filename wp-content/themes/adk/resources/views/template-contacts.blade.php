{{--
  Template Name: Contacts
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header')
  @while(have_posts()) @php the_post() @endphp
<div class="container contacts">
  	<div class="general">
   		<div class="row">
   			<div class="col-sm-6 general__container">
   				<div class="general__content ">
   					<h2 class="contacts__title"><?php echo pll_e('Kontaktinformācija', 'Contacts'); ?></h2>
   					<div class="row">
	   					<?php 
						$general = rwmb_meta('general');
						?>
						<div class="col-sm-6">
								<div class="general__content__item animate animate__fade-up">
									<img src="<?php echo get_template_directory_uri() . "/assets/images/Address_icon.svg";  ?>"/>
									<p class="general__content__title"><?php echo pll_e('Adrese:', 'Contacts'); ?></p>
									<p class="general__content__text"><?php echo $general['contacts_address']; ?></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="general__content__item animate animate__fade-up">
									<img src="<?php echo get_template_directory_uri() . "/assets/images/darba-laiks.svg";  ?>"/>
									<p class="general__content__title"><?php echo pll_e('Darba Laiks:', 'Contacts'); ?></p>
									<p class="general__content__text"><?php echo $general['contacts_open']; ?></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="general__content__item animate animate__fade-up">
									<img src="<?php echo get_template_directory_uri() . "/assets/images/E-mail_icon.svg";  ?>"/>
									<p class="general__content__title"><?php echo pll_e('E-pasts:', 'Contacts'); ?></p>
									<p class="general__content__text"><?php echo $general['contacts_email']; ?></p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="general__content__item animate animate__fade-up">
									<img src="<?php echo get_template_directory_uri() . "/assets/images/Address_icon.svg";  ?>"/>
									<p class="general__content__title"><?php echo pll_e('Tālrunis:', 'Contacts'); ?></p>
									<p class="general__content__text"><?php echo $general['contacts_phone']; ?></p>
								</div>
							</div>
						</div>
					</div>
   				<div>	
   			</div>
   		</div>
   		<div class="col-sm-6  general__container">
   			<div class="general__content ">
	   			<h2 class="contacts__title"><?php echo pll_e('Rekvizīti', 'Contacts'); ?></h2>
				<div class="row">
					<?php 
				$company = rwmb_meta('company');
				?>
					<div class="col-sm-12">
						<div class="general__content__item animate animate__fade-up">
							<img src="<?php echo get_template_directory_uri() . "/assets/images/Address_icon.svg";  ?>"/>
							<p class="general__content__title"><?php echo pll_e('Reģistrācijas numurs:', 'Contacts'); ?></p>
							<p class="general__content__text"><?php echo $company['reg_number']; ?></p>
							<p class="general__content__title"><?php echo pll_e('PVN Reģistra numurs:', 'Contacts'); ?></p>
							<p class="general__content__text"><?php echo $company['pvn_number']; ?></p>
							<p class="general__content__title"><?php echo pll_e('Banka:', 'Contacts'); ?></p>
							<p class="general__content__text"><?php echo $company['bank']; ?></p>
							<p class="general__content__title"><?php echo pll_e('Bankas:', 'Contacts'); ?></p>
							<p class="general__content__text"><?php echo $company['bank_code']; ?></p>
							<p class="general__content__title"><?php echo pll_e('Reģistrācijas numurs:', 'Contacts'); ?></p>
							<p class="general__content__text"><?php echo $company['bank_acc_number']; ?></p>
						</div>
					</div>
				</div>
			<div>
   		</div>
	</div>

	
</div>
<div class="contacts-section-2 container">
	<div class="row">
		<div class="col-sm-6">
			<h2 class="contacts__title"><?php echo pll_e('Komanda', 'Contacts'); ?></h2>
			
			<div class="row">
				<?php 
				$team = rwmb_meta('team');
				?>
				<?php foreach ($team as $value) : ?>
				<?php 
					$featured_images = wp_get_attachment_image_src($value['image'][0], 'medium');
          $featured_images_sad = wp_get_attachment_image_src($value['image_sad'][0], 'medium');
				?>
					<div class="col-sm-6 team animate animate__fade-up">
						<div class="team__image__wrap">
               <div class="team__image team__image--happy" style="background-image: url(<?php echo $featured_images[0]; ?>)"></div>
              <div class="team__image team__image--sad" style="background-image: url(<?php echo $featured_images_sad[0]; ?>)"></div>
            </div>
						<p class="team__pos"><?php echo $value['position']; ?></p>
						<p class="team__name"><?php echo $value['name']; ?></p>
						<a href="tel:<?php echo $value['phone']; ?>" class="team__phone"><?php echo pll_e('Mob:', 'Contacts'); ?> <?php echo $value['phone']; ?></a>
						<a href="mailto:<?php echo $value['email']; ?>" class="team__email"><?php echo $value['email']; ?></a>
					</div>
				<?php endforeach; ?>
			</div>
			
		</div>
		<div class="col-sm-6">
			
			<!-- contact form -->
			 <div class="contacts__form animate animate__fade-up">
            <form id="contact-form" method="POST">
                <h2 class="contacts__title"><?php echo pll__('Pieteikt vizīti', 'Form'); ?></h2>
                <div class="row">
                    <div class="col-sm-6">
                        <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('Vārds, Uzvārds', 'Form'); ?><span class="req">*</span></span>
                            <input type="text" name="name" id="name" class="required"/>
                        </label>
                        <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('Telefons', 'Form'); ?><span class="req">*</span></span>
                            <input type="text" name="phone" id="phone" class="required"/>
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('Uzņēmuma nosaukums', 'Form'); ?><span class="req">*</span></span>
                            <input type="text" name="company" id="company" class="required"/>
                        </label>
                        <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('Epasts', 'Form'); ?><span class="req">*</span></span>
                            <input type="text" name="email" id="email" class="required"/>
                        </label>
                    </div>
                </div>
                 <div class="row">
                	<div class="col-sm-12">
                        <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('Uzņēmuma darbības joma', 'Form'); ?></span>
                            <input type="text" name="companyType" id="companyType"/>
                        </label>
                    </div>
                </div>
                <div class="row">
                	<div class="col-sm-12">
                        <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('Vēlamais datums', 'Form'); ?></span>
                            <input type="text" name="date" id="date" class="datepicker" autocomplete="off"/>
                        </label>
                    </div>
                </div>
                <div class="row">
                	<div class="col-sm-12">
                       <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('Ziņas teksts', 'order-form'); ?><span class="req">*</span></span>
                            <textarea name="message" id="message" > </textarea>
                        </label>
                    </div>
                </div>
                    <div id="recaptcha_element"></div>
                    <div class="col-sm-12 button-container" style="text-align:center;">
                        <button class="button" class="contacts__form__submit"><?php echo pll__('Nosūtīt', 'Form'); ?></button>
                    </div>
                    <div class="col-sm-12 form-hidden">
                        <?php wp_nonce_field('contact-nonce', 'contact-nonce'); ?>
                        <?php $from = '';
                        if (isset($_GET['from'])) {
                            $from = strip_tags($_GET['from']);
                        } 
                        $send_to = rwmb_meta('adk_contact_email');
                        ?>
                        <input type="hidden" name="page_from" id="page_from" value="<?php echo $from; ?>" class="leave"/>
                        <input type="hidden" name="post_id" id="post_id" value="<?php echo $post->ID; ?>" class="leave"/>
                        <input type="hidden" name="action" id="action" value="contact_submit" class="leave"/>
                        <input type="hidden" name="send_to" id="send_to" value="<?php echo $send_to; ?>" class="leave"/>
                        <input type="text" name="contact-honey" id="contact-honey" class="hidden"/>
                        <div class="form__status hidden"></div>
                    </div>
</div>
            </form>
        </div>
		<!-- contact form -->
		</div>
		</div>
	</div>
	</div>
</div>
<div class="map">
<?php
$map = rwmb_get_value( 'adk_contact_map', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$pins = rwmb_meta('adk_contact_map_pin', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$pin = reset( $pins );

?>
<?php if (!empty($map)) : ?>
    <div class="contacts__map animate animate__fade-up">
      <div id="map-wrap"
             class="contacts__map__wrap"
               data-lng="<?php echo $map['longitude'] ?>"
             data-lat="<?php echo $map['latitude'] ?>"
             data-zoom="<?php echo $map['zoom']; ?>"
             data-pin="<?php echo $pin['url']; ?>"></div>
             </div>
    </div>
<?php endif; ?>
</div>
  @endwhile
  
@endsection


   			