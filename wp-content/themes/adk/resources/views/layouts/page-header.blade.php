<div class="page-header animate animate__fade-up">
	<div class="container">
		<?php // echo breadcrumbs(); ?>
		<div class="page-header__title">
			<h1><?php echo pll_e(page_title(), 'Titles'); ?></h1>
		</div>
	</div>
</div>