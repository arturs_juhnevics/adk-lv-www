  <!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>
    </div>
    @php do_action('get_footer') @endphp
   <script async data-main="<?php echo get_template_directory_uri(); ?>/assets/js/app.js?v=1.0.3"
                src="<?php echo get_template_directory_uri(); ?>/assets/js/require.min.js"></script>
    @include('partials.footer')
    @php wp_footer() @endphp
</html>
