<div class="wrap" style="font-size:14px; line-height:20px;">
<?php
	$fields = unserialize( $data->value );

	echo '<h3>' . $data->type . '</h3>';

	foreach ($fields as $key => $value) {

		if( gettype( $value ) == 'string' ){
			echo '<strong>' . ucfirst( str_replace('-', ' ', $key) ) . '</strong>: ';
			echo json_decode($value) . '<br />';
		}elseif( gettype( $value ) == 'array' ){
			echo '<strong>' . $value['key'] . ': </strong>';
			echo json_decode($value['value']) . '<br />';
		}
	}

	echo '<br /><div class="submitbox">';
	echo '<a style="float: left; margin-right:20px;" href="'.admin_url('admin.php?page=df-forms').'" class="button">Back to all</a>';
	echo '<div id="delete-action">';
	echo '<a class="submitdelete deletion" href="'.admin_url('admin.php?page=df-forms&form-delete='.$data->id).'">DELETE</a>';
	echo '</div></div>';
	?>
</div>