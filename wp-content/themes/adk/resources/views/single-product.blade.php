@extends('layouts.app')

@section('content')
 @include('layouts.page-header')
<div class="single-product">
 	@include('partials.product.single')	
</div>
@endsection