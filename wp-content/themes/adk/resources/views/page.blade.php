@extends('layouts.app')

@section('content')
@include('layouts.page-header')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
  @endwhile
@endsection
