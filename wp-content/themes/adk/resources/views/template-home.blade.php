{{--
  Template Name: Home
--}}
@extends('layouts.app')
@section('content')
<?php 
$enable_solutions = rwmb_meta('enable_solutions'); 
$enable_facts = rwmb_meta('enable_facts'); 

?>
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
    @include('partials.home.hero')
    <?php if( $enable_solutions == true) : ?>
    	@include('partials.home.solutions')
    <?php endif; ?>
    <?php if( $enable_facts == true) : ?>
    	@include('partials.home.facts')
    <?php endif; ?>
    @include('partials.home.partners')
  @endwhile
@endsection