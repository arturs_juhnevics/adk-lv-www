<?php 
$id = get_the_ID();
$gallery = rwmb_meta( 'product_gallery', array('size' => 'large') );
$term = wp_get_post_terms( get_the_ID(), 'brand-category');
$brandLogo = rwmb_meta( 'brand_cat_image', array( 'object_type' => 'term', 'limit' => 1 ), $term[0]->term_id );
reset($brandLogo);

$featImage = get_the_post_thumbnail_url($id, 'large');
$featImageThumb = get_the_post_thumbnail_url($id, 'medium');

$termCategory = wp_get_post_terms( get_the_ID(), 'product-category');
if($termCategory){
	$categoryUrl = get_term_link($termCategory[0]->term_id);
}

$solutions = get_post_meta( $id, 'solution' );


?>
<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<div class="gallery">
				<div class="gallery__main">
					<div class="gallery__main__inner">
						<div class="gallery__main__item">
								<img src="<?php echo $featImage; ?>" />
							</div>
						<?php foreach ($gallery as $image) { ?>
							<div class="gallery__main__item">
								<img src="<?php echo $image['url'] ?>" />
							</div>
						<?php } ?>
						
					</div>
				</div>
				<?php 
					$gallery = rwmb_meta( 'product_gallery', array('size' => 'medium') );
					$count = count($gallery);
				?>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="single-product__info">
				<?php if(isset($brandLogo[0])) :?>
					<img src="<?php echo $brandLogo[0]['url']; ?>">
				<?php endif; ?>
				<?php if($termCategory) : ?>
				<p class="single-product__info__item animate animate__fade-up"><span><?php echo pll__("Kategorija:"); ?></span><a class="link link--grey" href="<?php echo $categoryUrl; ?>"><?php echo $termCategory[0]->name; ?></a></p>
				<?php endif; ?>
				<?php if($solutions) : ?>
				<p class="single-product__info__item animate animate__fade-up"><span><?php echo pll__("Risinājumi:"); ?></span>
					<?php foreach ($solutions as $value) { ?>
						<?php 
							$url = get_the_permalink( $value );
							$title = get_the_title( $value )
						?>
						<a class="link link--grey" href="<?php echo $url; ?>"><?php echo $title; ?></a>
					<?php } ?>
					
				</p>
				<?php endif; ?>
				<hr>
				<p class="single-product__info__text animate animate__fade-up">
					<?php echo the_content(); ?>
				</p>
				<a href="<?php echo getTplPageURL("views/template-contacts.blade.php"); ?>" class="button"><?php echo pll__("Pieprasīt cenu"); ?></a> 
			</div>
		</div>
	</div>
<div class="accordion product-extra" id="accordionExample">
	<?php if( $solutions ) : ?>
	  <div class="card animate animate__fade-up">
	    <div class="card-header" id="headingOne">
	      <h2 class="mb-0">
	        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
	          <?php echo pll__("Risinājumi"); ?>
	        </button>
	      </h2>
	    </div>

	    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
	      <div class="card-body">
	        <div class="row">
	        	<?php foreach ($solutions as $value) { ?>
					<?php 
						$url = get_the_permalink( $value );
						$title = get_the_title( $value );
						$image = get_the_post_thumbnail_url( $value );
						$short = rwmb_meta('solution_short_text', array('size' =>'medium'), $value);
						$trimmed = wp_trim_words( $short, 30);
					?>
					<div class="col-sm-6">
						<div class="product-extra__solution-image">
							<img src="<?php echo $image; ?>">
						</div>
						<div class="product-extra__text">
							<h3><?php echo $title; ?></h3>
							<p><?php echo $trimmed ; ?></p>
							<a class="readmore-text" href="<?php $url; ?>"><?php echo pll__("Lasīt vairāk"); ?></a>
						</div>
					</div>
				<?php } ?>
	        </div>
	      </div>
	    </div>
	  </div>
	<?php endif; ?>
  <?php
  	$productExtra = rwmb_meta('product_extra');
  	$productTech = rwmb_meta('product_tech');
  	$downloads = rwmb_meta('product_files');
  ?>
  <?php if( isset($produtExtra) && $producExtra != '' ) : ?>
	  <div class="card animate animate__fade-up">
	    <div class="card-header" id="headingTwo">
	      <h2 class="mb-0">
	        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
	          <?php echo pll__("Papildus"); ?>
	        </button>
	      </h2>
	    </div>
	    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
	      <div class="card-body">
	      	<div class="text">
	       		<?php echo $productExtra; ?>
	       </div>
	      </div>
	    </div>
	  </div>
  <?php endif; ?>
  <?php if( isset($productTech) && $productTech != '' ) : ?>
	  <div class="card animate animate__fade-up">
	    <div class="card-header" id="headingThree">
	      <h2 class="mb-0">
	        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
	          <?php echo pll__("Tehniskie parametri"); ?>
	        </button>
	      </h2>
	    </div>
	    <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
	      <div class="card-body">
	      		<div class="text">
	      			<?php echo $productTech; ?>
	      		</div>
	        	
	      </div>
	    </div>
	  </div>
  <?php endif; ?>
  <?php if( $downloads ) : ?>
	  <div class="card animate animate__fade-up">
	    <div class="card-header" id="headingFour">
	      <h2 class="mb-0">
	        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
	          <?php echo pll__("Lejuplādes"); ?>
	        </button>
	      </h2>
	    </div>
	    <div id="collapseThree" class="collapse show" aria-labelledby="headingFour" data-parent="#accordionExample">
	      <div class="card-body">
	     	<?php foreach ($downloads as $value) { ?>
	     	<?php 
	     		$ext = pathinfo($value['path'], PATHINFO_EXTENSION); 
	     		$extname = 'file';
	     		$fileClass = 'regular';
	     		if($ext == 'xls'){
					$extname = 'xls';
	     		}
	     		if($ext == 'pdf'){
					$extname = 'pdf';
					$fileClass = 'pdf';
	     		}
	     	?>
	     		<a href="<?php echo $value['url']; ?>" target="_blank" class="downloads__item <?php echo $fileClass; ?>">
	     			<span class="downloads__item__ext"><?php echo $extname; ?></span>
	     			<span class="downloads__item__name"><?php echo $value['name']; ?></span>
	     			<span class="downloads__item__size">1.2 MB</span>
	     		</a>
	     	<?php } ?>
	      </div>
	    </div>
	  </div>
  <?php endif; ?>
</div>
	
	<div class="related-posts">
      <div class="related-posts__nav">
        <h2 class=""><?php echo pll__('Saistītie prodokti', 'Product') ?></h2>
        <div class="related-posts__nav__controls slick-controls mob-hidden">
          <span class="related-prev"></span>
          <span class="related-next"></span>
        </div>
      </div>
      <div class="post-slider">
        <?php 

          $termId = $termCategory[0]->term_id;

          if( isset($termCategory[1])) :
          	$termId = $termCategory[1]->term_id;
          endif; 

          $query = new WP_Query( 
            array( 
              'post_type' => 'product',
              'posts_per_page'=> 12, 
              'post__not_in' => array(get_the_ID()),
              'tax_query' => array(
			        array(
			            'taxonomy' => 'product-category',
			            'field'    => 'id',
			            'terms'    => $termId,
			        ),
			    ),
            ) 
          );
          ?>
          <?php while ($query->have_posts()) : $query->the_post(); ?> 
          <?php 
          	$image = get_the_post_thumbnail_url();
			$title = get_the_title(); 
			$url = get_the_permalink();
          ?>
      
			@include('partials.product.product-list') 
		
          <?php endwhile; ?>
      </div>
    </div>

</div>
