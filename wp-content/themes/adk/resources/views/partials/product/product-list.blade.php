<?php 
//$terms = wp_get_post_terms( get_the_ID(), 'product-category');
?>
<a href="<?php echo get_the_permalink( ); ?>">
<div class="product-item animate animate__fade-up">
	<!-- <div class="product-item__term">
		<?php //if( $terms ) : ?>
		<p><?php// echo $terms[0]->name; ?></p>
		<?php //endif; ?>
	</div> -->
	<div class="product-item__image" style="background-image:url(<?php echo get_the_post_thumbnail_url() ?>);">
	</div>
	<p class="product-item__name">
		<?php echo get_the_title( ); ?>
	</p>
</div>
</a>