<div class="container facts block">
	<div class="block__heading">
		<div class="row">
			<div class="col-6 block__heading__left animate animate__fade-up"><h2><?php echo rwmb_meta('facts_title'); ?></h2></div>
			<div class="col-6 block__heading__right animate animate__fade-up"><a class="button--read-more" href="<?php echo rwmb_meta('facts_url'); ?>"><?php echo pll__('Par mums'); ?></a></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<p class="facts__short"><?php echo rwmb_meta('facts_text'); ?></p>
			<?php 
				$facts = rwmb_meta('facts');
				
			?>
			<?php foreach ($facts as $value) { ?>
				<div class="facts__list animate animate__fade-up">
				<div class="facts__list__item clearfix">
					<div class="facts__list__item__number"><div class="facts__list__item__number__inner"><?php echo $value['number']; ?></div></div>
					<div class="facts__list__item__desc">
						<p class="facts__list__item__desc__title"><?php echo $value['title']; ?></p>
						<p class="facts__list__item__desc__text"><?php echo $value['text']; ?></p>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="col-sm-6">
			<?php 
				$image = rwmb_meta('facts_image', array( 'limit' => 1, 'size' => 'large'));
				$image = reset( $image );
			?>
			<div class="facts__image animate animate__fade-up" style="background-image: url(<?php echo $image['url']; ?>)"></div>
		</div>
	</div>
</div>