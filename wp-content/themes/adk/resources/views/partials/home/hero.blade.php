@php
$slides = get_posts( array(
    'post_type' => 'slider',
    'numberposts' => -1,
    'post_status' => 'publish',
) );

@endphp
<div class="hero">
@foreach ($slides as $slide)
	@php
	$image = get_the_post_thumbnail_url($slide->ID);

	$title = get_post_meta($slide->ID, 'slider_title');
	$text = get_post_meta($slide->ID, 'slider_text');
	$buttonText = get_post_meta($slide->ID, 'slider_button');
	$buttonUrl = get_post_meta($slide->ID, 'slider_button_url');

	@endphp
	
	<div class="hero__item" style="background-image: url({{ $image }})">
		@if($buttonUrl)
		<a href="{{$buttonUrl[0]}}" style="
			position: absolute;
			height: 100%;
			width: 100%;
			top: 0;
			left: 0;
			z-index: 9999;
		">
		@endif
			<div class="container">
				<div class="hero__item__inner">
					<h2 class="hero__title">{{ $title[0] }}</h2>
						<div class="hero__text">{{ $text[0] }}</div>
						@if($buttonText)
							<p class="button">{{ $buttonText[0] }}</p> 
						@endif
				</div>
			</div>
		@if($buttonUrl)
		</a>
		@endif
	</div>
@endforeach
</div>