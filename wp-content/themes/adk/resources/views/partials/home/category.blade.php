@php
$terms = get_terms( array(
    'taxonomy' => 'product-category',
    'hide_empty' => false,
    'parent' => 0,
) );
$term_id = '';
if(is_tax()){
	$term_id = get_queried_object()->term_id;
}

@endphp
<div class="product-categories">
	<div class="container">
		<div class="row">
			@foreach($terms as $term)
				@php
				$url = "";
				$title = $term->name;
				$image = rwmb_meta('product_cat_image', array( 'object_type' => 'term', 'limit' => 1 ), $term->term_id );
		
				@endphp
				<?php $isActive = ''; ?>
				<?php if($term_id == $term->term_id): ?>
					<?php $isActive = 'active'; ?>
				<?php endif; ?>
					<div class="col-sm-3 col-6">
						<a href="<?php echo get_term_link($term->term_id); ?>">
						<div class="product-categories__item animate animate__fade-up <?php echo $isActive; ?>">
					
							<?php echo file_get_contents($image[0]['url']) ?>
							<p class="product-categories__title">{{ $title }}</p>
						</div>
						 </a>
					</div>
			@endforeach
		</div>
	</div>
</div>
