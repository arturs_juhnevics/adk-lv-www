<?php 
	$id = get_the_ID();
	$image = get_the_post_thumbnail_url($id, 'medium');
	$url = get_the_permalink();
?>
<div class="solution-list__item col-sm-6 col-lg-3" >
	<a href="<?php echo $url; ?>">
	<div class="solution-list__item__inner animate animate__fade-up" style="background-image: url(<?php echo $image; ?>)">
		<div class="overlay"></div>
		<p class="solution-list__item__title"><?php echo get_the_title(); ?></p>
		<a href="<?php echo $url; ?>" class="slick-next slick-arrow"></a>
	</div>
	</a>

</div>