gulp = require 'gulp'
uglify = require 'gulp-uglify'
minifyCss = require 'gulp-clean-css'
stripCssComments = require 'gulp-strip-css-comments'
sass = require 'gulp-sass'
sourcemaps = require 'gulp-sourcemaps'
autoprefixer = require 'gulp-autoprefixer'

AUTOPREFIXER_BROWSERS = [
  'ie >= 9'
  'ie_mob >= 10'
  'ff >= 3.5'
  'chrome >= 4.0'
  'safari >= 3.1'
  'opera >= 10.5'
  'ios >= 7'
  'android >= 4.4'
  'bb >= 10'
]

swallowError = (error) ->
  console.log error.toString()
  @emit 'end'
  return



paths =
  build:
    js: '../themes/adk/resources/assets/js/modules/min'
    css: '../themes/adk/resources/assets/styles'
  watch:
    js:[
      '../themes/adk/resources/assets/js/modules/*.js'
      '../themes/adk/resources/assets/js/app.js'
    ]
    sass:[ 
      '../themes/adk/resources/assets/sass/*.scss'
      '../themes/adk/resources/assets/sass/**/*.scss'
    ]
  source:
    sass: [
      '../themes/adk/resources/assets/sass/main.scss'
      '../themes/adk/resources/assets/sass/admin-style.scss'
      '../themes/adk/resources/assets/sass/editor-style.scss'
    ]


gulp.task 'js', ->
  gulp.src paths.watch.js
    .pipe uglify()
      .on('error', swallowError)
    .pipe gulp.dest paths.build.js

gulp.task 'default', ->
  gulp.start 'js', 'sass'

gulp.task 'sass', ->
  gulp.src paths.source.sass
    .pipe sourcemaps.init({loadMaps:true})
    .pipe sass()
      .on('error', swallowError)
    .pipe stripCssComments()
    .pipe autoprefixer({browsers: AUTOPREFIXER_BROWSERS})
    .pipe minifyCss()
      .on('error', swallowError)
    .pipe sourcemaps.write('/')
    .pipe gulp.dest paths.build.css


gulp.task 'watch', ->
  gulp.start 'default'

  gulp.watch paths.watch.js, ['js']
  gulp.watch paths.watch.sass, ['sass']
